<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return '<div style="text-align:center; margin-top: 20%; font-weight: 5; height: 10vh; font-size: 45px; font-family: "Raleway", sans-serif;">Here return landing page <br> <img width="100" height="100" src="https://www.gustore.cl/img/estampados/444/444.png"><div>';
});

Auth::routes();
Route::group(['middleware' => 'roles:1-2', 'prefix' => 'plataforma'], function () {

  Route::get('/', function () {
      return view('plataforma.dashboard.index');
  });
  Route::resource('Integrante','IntegranteController');
  Route::resource('Usuarios','UserController');
  Route::resource('Servicio','ServicioController');
  Route::resource('States', 'StatesController');
  Route::resource('Cities', 'CitiesController');
  Route::resource('Vehicle_Type', 'Vehicle_TypeController');
  Route::resource('Vehicle', 'VehicleController');
  Route::resource('Manufacturers', 'ManufacturerController');
  Route::resource('Payment_Type', 'Payment_TypeController');
  Route::resource('Passenger', 'PassengerController');
  Route::resource('Vehicle_Model', 'Vehicle_ModelController');
  Route::resource('Driver', 'DriverController');
  Route::resource('Organization', 'OrganizationController');
  Route::resource('Agreement', 'AgreementController');
});


Route::get('RegistrarConductor', 'DriverController@registerDriverForm');// Register new driver view
Route::post('RegistrarConductor', 'DriverController@registerDriverRequest');// Register new driver request
