-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-06-2019 a las 18:07:01
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdserv`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bookings`
--

CREATE TABLE `bookings` (
  `booking_id` int(10) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `hour` time NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `journey_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `city_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ing` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `km_range` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `votes` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`city_id`, `name`, `lat`, `ing`, `km_range`, `state_id`, `status`, `votes`, `created_at`, `updated_at`) VALUES
(1, 'Alvaro', 'ninguna', 'ninguna', 20, 1, 'ninguno', 9, '2019-06-23 22:58:13', '2019-06-23 23:07:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `drivers`
--

CREATE TABLE `drivers` (
  `driver_id` int(10) UNSIGNED NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ing` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `driver_licence` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_licence_img_src` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_expiry_date` date NOT NULL,
  `banck_account` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` varchar(700) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicle_type_id` int(11) NOT NULL,
  `operating_area` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_address` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grades` int(11) NOT NULL,
  `speed` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `driver__scores`
--

CREATE TABLE `driver__scores` (
  `driver_score_id` int(10) UNSIGNED NOT NULL,
  `driver_id` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorite__locations`
--

CREATE TABLE `favorite__locations` (
  `favorite_location_id` int(10) UNSIGNED NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `journeys`
--

CREATE TABLE `journeys` (
  `journey_id` int(10) UNSIGNED NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `cancel_motive` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cancel_date` date NOT NULL,
  `duration` int(11) NOT NULL,
  `distance_in_mts` double(8,2) NOT NULL,
  `city_id` int(11) NOT NULL,
  `pick_waypoint` int(11) NOT NULL,
  `drop_waypoint` int(11) NOT NULL,
  `remarks` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `booling_id` int(11) NOT NULL,
  `vehicle_type` int(11) NOT NULL,
  `vehicle_id` int(11) NOT NULL,
  `complete_date` date NOT NULL,
  `cancel_person` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `arrive_date` date NOT NULL,
  `starter_date` date NOT NULL,
  `price_per_distance` double(8,2) NOT NULL,
  `price_per_minute` double(8,2) NOT NULL,
  `total_cancelation` double(8,2) NOT NULL,
  `total` double(8,2) NOT NULL,
  `payment_id` int(11) NOT NULL,
  `max_price_per_distance` double(8,2) NOT NULL,
  `min_price_per_distance` double(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manufacturers`
--

CREATE TABLE `manufacturers` (
  `manufacturer_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `manufacturers`
--

INSERT INTO `manufacturers` (`manufacturer_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Ford', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_06_13_164841_create_states_table', 1),
(3, '2019_06_13_165628_create_cities_table', 1),
(4, '2019_06_13_170339_create_drivers_table', 1),
(5, '2019_06_13_171504_create_passengers_table', 1),
(6, '2019_06_13_172028_create_vehicles_table', 1),
(7, '2019_06_13_172048_create_vehicle__types_table', 1),
(8, '2019_06_13_172121_create_payment__types_table', 1),
(9, '2019_06_13_172144_create_payments_table', 1),
(10, '2019_06_13_172213_create_driver__scores_table', 1),
(11, '2019_06_13_172243_create_passenger__scores_table', 1),
(12, '2019_06_13_175625_create_waypoints_table', 1),
(13, '2019_06_13_175700_create_favorite__locations_table', 1),
(14, '2019_06_13_175939_create_journeys_table', 1),
(15, '2019_06_13_184606_create_bookings_table', 1),
(16, '2019_06_13_184606_create_roles_table', 1),
(17, '2019_06_24_153745_create_manufacturers_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passengers`
--

CREATE TABLE `passengers` (
  `passenger_id` int(10) UNSIGNED NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `int` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `passenger__scores`
--

CREATE TABLE `passenger__scores` (
  `passenger_score_id` int(10) UNSIGNED NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `calificacion` int(11) NOT NULL,
  `comment` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payments`
--

CREATE TABLE `payments` (
  `payment_id` int(10) UNSIGNED NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `payment_type_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double(8,2) NOT NULL,
  `token` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment__types`
--

CREATE TABLE `payment__types` (
  `payment_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `rol_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`rol_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', NULL, NULL),
(2, 'Supervisor', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `state_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `short_name` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`state_id`, `name`, `short_name`, `created_at`, `updated_at`) VALUES
(1, 'Michoacán', 'Mich', '2019-06-18 21:45:04', '2019-06-18 21:45:04'),
(3, 'Jalisco', 'Jal', '2019-06-18 21:48:41', '2019-06-18 21:48:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_src` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `login_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fcm_token` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `android_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rol_id` int(11) DEFAULT NULL,
  `last_login` date DEFAULT NULL,
  `phone` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `app_android_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ios_android_version` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `baja` char(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `img_src`, `login_type`, `facebook_id`, `fcm_token`, `city_id`, `android_version`, `ios_version`, `rol_id`, `last_login`, `phone`, `app_android_version`, `ios_android_version`, `baja`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Zavala', 'otherazf007@gmail.com', '$2y$10$LfuJl1kyTNPYL6U71yPDg.l7TNFaNOB0Kay4L6TjcoaMUaOh206fC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '0', 'STSYKKqpO8FL8ZnDNndGZZQ6UA8wxC81AL3JtA5UYI2LXHwRR3jCTaERtt7O', '2019-06-18 21:30:29', '2019-06-18 21:40:52'),
(2, 'Alejandro', 'azf007@gmail.com', '1234567890', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `vehicle_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `manufacter_id` int(11) NOT NULL,
  `color` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `insurance_no` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `insurance_expiry_date` date NOT NULL,
  `city_id` int(11) NOT NULL,
  `road_taxt` double(8,2) NOT NULL,
  `plate` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicule_type_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicle__types`
--

CREATE TABLE `vehicle__types` (
  `vehicle_type_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `max_persons` int(11) NOT NULL,
  `base_fare` double(8,2) NOT NULL,
  `min_fare` double(8,2) NOT NULL,
  `price_per_min` double(8,2) NOT NULL,
  `cancellation_price` double(8,2) NOT NULL,
  `img_src` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_price_per_distance` double(8,2) NOT NULL,
  `max_price_per_distance` double(8,2) NOT NULL,
  `base_fare_per_minute` double(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicle__types`
--

INSERT INTO `vehicle__types` (`vehicle_type_id`, `name`, `max_persons`, `base_fare`, `min_fare`, `price_per_min`, `cancellation_price`, `img_src`, `min_price_per_distance`, `max_price_per_distance`, `base_fare_per_minute`, `created_at`, `updated_at`) VALUES
(3, 'Vehiculo', 4, 1.00, 1.00, 1.00, 1.00, 'img/vehicle_type/vehicle_type3.jpg', 1.00, 1.00, 1.00, '2019-06-24 04:50:03', '2019-06-24 04:50:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `waypoints`
--

CREATE TABLE `waypoints` (
  `waypoint_id` int(10) UNSIGNED NOT NULL,
  `lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ing` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_place_id` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `journey_id` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `street` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `col` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`booking_id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indices de la tabla `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`driver_id`);

--
-- Indices de la tabla `driver__scores`
--
ALTER TABLE `driver__scores`
  ADD PRIMARY KEY (`driver_score_id`);

--
-- Indices de la tabla `favorite__locations`
--
ALTER TABLE `favorite__locations`
  ADD PRIMARY KEY (`favorite_location_id`);

--
-- Indices de la tabla `journeys`
--
ALTER TABLE `journeys`
  ADD PRIMARY KEY (`journey_id`);

--
-- Indices de la tabla `manufacturers`
--
ALTER TABLE `manufacturers`
  ADD PRIMARY KEY (`manufacturer_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `passengers`
--
ALTER TABLE `passengers`
  ADD PRIMARY KEY (`passenger_id`);

--
-- Indices de la tabla `passenger__scores`
--
ALTER TABLE `passenger__scores`
  ADD PRIMARY KEY (`passenger_score_id`);

--
-- Indices de la tabla `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`payment_id`);

--
-- Indices de la tabla `payment__types`
--
ALTER TABLE `payment__types`
  ADD PRIMARY KEY (`payment_type_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`state_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`vehicle_id`);

--
-- Indices de la tabla `vehicle__types`
--
ALTER TABLE `vehicle__types`
  ADD PRIMARY KEY (`vehicle_type_id`);

--
-- Indices de la tabla `waypoints`
--
ALTER TABLE `waypoints`
  ADD PRIMARY KEY (`waypoint_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `bookings`
--
ALTER TABLE `bookings`
  MODIFY `booking_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `drivers`
--
ALTER TABLE `drivers`
  MODIFY `driver_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `driver__scores`
--
ALTER TABLE `driver__scores`
  MODIFY `driver_score_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `favorite__locations`
--
ALTER TABLE `favorite__locations`
  MODIFY `favorite_location_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `journeys`
--
ALTER TABLE `journeys`
  MODIFY `journey_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `manufacturers`
--
ALTER TABLE `manufacturers`
  MODIFY `manufacturer_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `passengers`
--
ALTER TABLE `passengers`
  MODIFY `passenger_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `passenger__scores`
--
ALTER TABLE `passenger__scores`
  MODIFY `passenger_score_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payments`
--
ALTER TABLE `payments`
  MODIFY `payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `payment__types`
--
ALTER TABLE `payment__types`
  MODIFY `payment_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `rol_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `state_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `vehicle_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `vehicle__types`
--
ALTER TABLE `vehicle__types`
  MODIFY `vehicle_type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `waypoints`
--
ALTER TABLE `waypoints`
  MODIFY `waypoint_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
