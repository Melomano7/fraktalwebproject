<!DOCTYPE html>
<html class=" ">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="https://www.shortformapps.com/wp-content/uploads/2016/06/BLUE-ICON.png">
    <title>@yield('title', 'Plataforma : Administración')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Laravel boilerplate" name="description" />
    <meta content="Fraktalweb" name="author" />

    <!-- CORE CSS FRAMEWORK - START -->
    {{Html::style('assets/plugins/pace/pace-theme-flash.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/bootstrap/css/bootstrap.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/bootstrap/css/bootstrap-theme.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/fonts/font-awesome/css/font-awesome.css', array('media'=>'screen'))}}
    {{Html::style('assets/css/animate.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css', array('media'=>'screen'))}}
    <!-- CORE CSS FRAMEWORK - END -->

    {{Html::style('assets/plugins/jquery-ui/smoothness/jquery-ui.min.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/datepicker/css/datepicker.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/daterangepicker/css/daterangepicker-bs3.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/daterangepicker/css/daterangepicker-bs3.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datetimepicker/css/datetimepicker.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/timepicker/css/timepicker.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/select2/select2.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/icheck/skins/all.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/multi-select/css/multi-select.css', array('media' => 'screen')) }}
    {{Html::style('assets/plugins/prettyphoto/prettyPhoto.css', array('media' => 'screen')) }}
    {{Html::style('assets/plugins/tagsinput/css/bootstrap-tagsinput.css', array('media' => 'screen')) }}

    {{Html::style('assets/plugins/inputmask/jquery.inputmask.bundle.min.js', array('media'=>'screen'))}}


    <!-- CORE CSS TEMPLATE - START -->
    {{Html::style('assets/css/style.css', array('media'=>'screen'))}}
    {{Html::style('assets/css/responsive.css', array('media'=>'screen'))}}
    {{Html::style('css/main.css', array('media'=>'screen'))}}
    <!-- CORE CSS TEMPLATE - END -->

    @yield('head')
  
</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->

<body class=" ">
    <!-- START CONTAINER -->
    <div class="page-container row-fluid">
        <!-- START CONTENT -->
        <section id="" class="">
            <section class="wrapper" style='padding-top:60px;display:inline-block;width:100%;'>
                <div class='col-lg-6 col-md-12 col-sm-12 col-xs-12'>
                    <div class="page-title">
                        <div class="">
                            {{-- <h1 class="title">Registrar un nuevo conductor</h1> --}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-xs-12 col-sm-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
                    <div class="row">
                        <div class="col-md-12">
                              @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
                            <section class="box">
                                <div class="content-body">
                                    <div class="row">
                                        <div class="col col-lg-2 col-lg-offset-5 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-6 col-xs-offset-3">
                                            <img src="https://image.freepik.com/free-vector/car-logo-template_9298-22.jpg"
                                            alt="logotipo" width="100%">
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h1>Registrar nuevo conductor</h1><br>

                                            {{ Form::open( array('url' => 'RegistrarConductor', 'method' => 'POST','id' => 'icon_validate', 'class' => '', 'enctype' => 'multipart/form-data') ) }}


                                            <div class="form-group">
                                                {{ Form::label ('name', 'Nombre *', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('name', '') }}
                                                    {{ Form::text('name', '', ['class' => 'form-control name', 'required' => 'required']) }}
                                                    @if($errors->first('name'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('name') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                {{ Form::label ('email', 'Email *', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('email', '') }}
                                                    {{Form::email('email', '', ['class' => 'form-control', 'required' => 'required']) }}
                                                    @if($errors->first('email'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('email') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
    
                                            {{-- La contraseña la debemos generar nosotros, ellos la editaran después --}}
                                            <div class="form-group">
                                                {{ Form::label ('phone', 'Teléfono *', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('phone', '') }}
                                                    {{Form::text('phone', '', ['class' => 'form-control', 'data-mask' => 'phone', 'required' => 'required']) }}
                                                    @if($errors->first('phone'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('phone') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                          
                                            <div class="form-group">
                                                {{ Form::label ('driver_licence', 'Licencia de Manejo *', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('driver_licence', '') }}
                                                    {{Form::text('driver_licence', '', ['class' => 'form-control', 'required' => 'required']) }}
                                                    @if($errors->first('driver_licence'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('driver_licence') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label for="">Fecha de vencimiento de la licencia*</label>
                                                <input type="text" class="form-control datepicker"data-format="D, dd MM yyyy" value="" name="license_expiry_date" >
                                            </div>


                                            <div class="form-group">
                                                {{ Form::label ('banck_account', 'Cuenta Bancaria*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('banck_account', '') }}
                                                    {{Form::text('banck_account', '', ['class' => 'form-control', 'required' => 'required','data-mask' => '9999999999999999']) }}
                                                    @if($errors->first('banck_account'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('banck_account') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                             <div class="form-group">
                                                {{ Form::label ('about', 'Acerca de*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('about', '') }}
                                                    {{Form::text('about', '', ['class' => 'form-control', 'required' => 'required']) }}
                                                    @if($errors->first('about'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('about') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                {{ Form::label ('home_address', 'Dirección*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('home_address', '') }}
                                                    {{Form::text('home_address', '', ['class' => 'form-control', 'required' => 'required']) }}
                                                    @if($errors->first('home_address'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('home_address') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>


                                              <div class="form-group">
                                                <label for="">Estado*</label>
                                                <select class="" name="state_id" id="s2example-1">
                                                    @foreach($estado as $id => $name)
                                                        <option value="{{$id}}">{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div> 

                                            <div class="form-group">
                                                <label for="">Ciudad*</label>
                                                <select class="" name="city_id" id="s2example-2">
                                                   
                                                </select>
                                            </div> 


                                            <div class="form-group">
                                                <label for="">Tipo de chofer*</label>
                                                <select class="form-control" name="driver_type" id="driver_type_select">
                                                    <option value="Propietario">Propietario</option>
                                                    <option value="Chofer">Chofer</option>
                                                    
                                                </select>
                                            </div>



                                            <div id="new-vehicle-form">
                                                
                                                <div class="form-group">
                                                {{ Form::label ('title', 'Titulo*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('title', '') }}
                                                    {{Form::text('title', '', ['class' => 'form-control']) }}
                                                    @if($errors->first('title'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('title') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                               

                                                {{--
                                                     Se supone que si es propietario del vechiculo
                                                     no debe preguntar quien es el dueño
                                                     El campo de driver_id en la tabla de vechicle es para identificar el dueño
                                                     ¿Porque se pide el dueño, si apenas se va a registrar el dueño?

                                                --}}
                                                {{-- <div class="form-group">
                                                    <label for="">Dueño*</label>
                                                    <select class="form-control" name="driver_id" id="driver_id_select">
                                                        @foreach($con as $id => $name)
                                                            <option value="{{$id}}">{{$name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div> --}}

                                                <div class="form-group">
                                                    <label for="">Estatus*</label>
                                                    <select class="form-control" name="status">
                                                        <option value="active">Activo</option>
                                                        <option value="inactive">Inactivo</option>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                <label for="">Fabricante*</label>
                                                <select class="" name="manufacturer_id" id="s2example-3">
                                                    @foreach($manufacturer as $id => $name)
                                                        <option value="{{$id}}">{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                               
                                                <div class="form-group">
                                                <label for="">Modelo*</label>
                                                <select class="" name="vehicle_model_id" id="s2example-4">
                                                    
                                                </select>
                                            </div>

                                               

                                                <div class="form-group">
                                                    <label for="">Año*</label>
                                                    <select class="form-control" name="year" id="year_select">
                                                        <?php
                                                            for($i=date('o'); $i>=2010; $i--){
                                                                if ($i == date('o'))
                                                                    echo '<option value="'.$i.'" selected>'.$i.'</option>';
                                                                else
                                                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                
                                                 <div class="form-group">
                                                {{ Form::label ('color', 'Color*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('color', '') }}
                                                    {{Form::text('color', '', ['class' => 'form-control']) }}
                                                    @if($errors->first('color'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('color') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                               
                                                <div class="form-group">
                                                {{ Form::label ('insurance_no', 'Numero de seguro*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('insurance_no', '') }}
                                                    {{Form::text('insurance_no', '', ['class' => 'form-control']) }}
                                                    @if($errors->first('insurance_no'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('insurance_no') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                               

                                                <div class="form-group">
                                                    <label for="">Fecha de vencimiento del seguro*</label>
                                                    <input type="text" class="form-control datepicker" data-format="D, dd MM yyyy"  value="" name="insurance_expiry_date">
                                                </div>


                                                {{--
                                                    Aqui la ciudad está de mas porque arriba ya se seleccionó para el conductor
                                                    Solo en el controloador insertar la misma ciudad para el vehiculo
                                                --}}
                                                
                                                {{-- <div class="form-group">
                                                    <label for="">Ciudad*</label>
                                                    <select class="form-control" name="city_id" id="s2example-1">
                                                        @foreach($ciudad as $id => $name)
                                                            <option value="{{$id}}">{{$name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div> --}}
                                                
                                                <div class="form-group">
                                                {{ Form::label ('road_taxt', 'Impuesto de Circulación*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('road_taxt', '') }}
                                                    {{Form::text('road_taxt', '', ['class' => 'form-control']) }}
                                                    @if($errors->first('road_taxt'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('road_taxt') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>

                                                <div class="form-group">
                                                {{ Form::label ('plate', 'Placas*', ['class' => 'form-label']) }}
                                                <div class="controls">
                                                    <i class=""></i>
                                                    {{ Form::hidden('plate', '') }}
                                                    {{Form::text('plate', '', ['class' => 'form-control']) }}
                                                    @if($errors->first('plate'))
                                                        <div class="alert alert-error alert-dismissible fade in">
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                            </button>
                                                            {{ $errors->first('plate') }}
                                                        </div>
                                                    @endif
                                                </div>
                                            </div>
                                                
                                              
                            

                            <div class="form-group">
                            <label for="">Tipo*</label>
                                <select class="form-control" name="vehicle_type_id" id="vehicle_type_id_select">
                                                        @foreach($type as $id => $name)
                                                            <option value="{{$id}}">{{$name}}</option>
                                                        @endforeach
                                                    </select>
                        </div>
                        

                         <div class="form-group">
                        {{ Form::label ('imagen', 'Imagen *', ['class' => 'form-label']) }}
                        <div class="controls">
                            <i class=""></i>
                            
                                {{ Form::file ('img_src', ['class' => 'custom-file-input', 'driver_id' => 'img_src']) }}
                            
                            @if($errors->first('img_src'))
                                <div class="alert alert-error alert-dismissible fade in">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    {{ $errors->first('img_src') }}
                                </div>
                            @endif
                        </div>
                        <p>Medidas sugeridas: 1220 * 790</p>
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" style="top:-60px;">
                            {{ Html::image('', '', array('class' => 'img-thumbnail img-responsive', 'alt' => '140x140', 'id' => 'imagenChange', 'data-holder-rendered' => 'true')) }}
                            <a class="preview previewGasto" href="" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                        </div>
                    </div>

                    

                                            </div>

                                            <div id="vehicle-select" hidden>

                                                {{--  
                                                    Tal vez aqui se pueda agregar compañia
                                                    si la misma agencia de taxis tiene varias compañias
                                                    sería mas facil identificar los carros. Ejemplo:
                                                    taxis morelia || taxis patzcuaro || taxis uruapan

                                                    Despues de seleccionar la compañia, seleccionar el vechiculo que se carga en base a las compañias
                                                    Similar a lo que se hace con los estados y ciudades
                                                --}}
                                                {{-- <div class="form-group">
                                                    <label for="">Compañia </label> <br>
                                                    <select class="form-control" name="state_id" id="state_id_select">

                                                    </select>
                                                </div> --}}

                                                <div class="form-group">
                                                    <label for="">Selecciona el vehiculo*</label>
                                                    <select class="form-control" name="vehicle_id" id="vehicle_id_select">
                                                        @foreach($vehicles as $id => $plate)
                                                            <option value="{{$id}}">{{$plate}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>


                                                <div class="text-right" style="margin-top:100px;">
                                                {{ Form::submit('Registrar', ['class' => 'btn btn-success', 'type' => 'submit', 'driver_id' => 'guardar']) }}
                                            </div>
                                            {{ csrf_field() }}
                                            {{ Form::close() }}



                                        </div>
                                    </div>
                                </div>


                            </section>

                        </div>
                    </div>




                </div>



            </section>
        </section>
        <!-- END CONTENT -->


    </div>
    <!-- END CONTAINER -->
    <!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

    @section('footer')

    <!-- CORE JS FRAMEWORK - START -->
    {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js') }}
    <!-- {{-- HTML::script('assets/js/jquery-1.11.2.min.js') --}} -->
    {{ HTML::script('assets/js/jquery.easing.min.js')}}
    {{ HTML::script('assets/plugins/bootstrap/js/bootstrap.min.js')}}
    {{ HTML::script('assets/plugins/pace/pace.min.js')}}
    {{ HTML::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}
    {{ HTML::script('assets/plugins/viewport/viewportchecker.js') }}
    <!-- CORE JS FRAMEWORK - END -->

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
    {{ HTML::script('assets/plugins/datepicker/js/datepicker.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/daterangepicker/js/moment.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/daterangepicker/js/daterangepicker.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/timepicker/js/timepicker.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/datetimepicker/js/datetimepicker.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/jquery-validation/js/jquery.validate.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/jquery-validation/js/additional-methods.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/js/form-validation.js', ["type" => "text/javascript"]) }}
    {{ Html::script('assets/plugins/jquery-ui/smoothness/jquery-ui.min.js', ["type" => "text/javascript"]) }}
    {{ Html::script('assets/plugins/icheck/icheck.min.js', ["type" => "text/javascript"]) }}
    {{ Html::script('assets/plugins/select2/select2.min.js', ["type" => "text/javascript"]) }}
    {{ HTML::script('assets/plugins/multi-select/js/jquery.multi-select.js', ["type" => "text/javascript"])}}

    <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


    <!-- CORE TEMPLATE JS - START -->
    {{ HTML::script('assets/js/scripts.js') }}
    <!-- END CORE TEMPLATE JS - END -->



    @show

    <script type="text/javascript">
        $(function () {

            // Getter
            var getTypeDriver = function(){
                return $('#driver_type_select').val();
            }

            var getStateId = function(){
                return $("#s2example-1").val();
            }
           
           var getManufacturerId = function(){
                return $("#s2example-3").val();
            }

           var getTypeDriver = function(){
                return $('#driver_type_select').val();
            }


            var changeVehicleDriverForm = function(){
                if( getTypeDriver() == "Propietario"){
                    $("#new-vehicle-form").show(600);
                    $("#vehicle-select").hide(600);
                }else{
                    $("#new-vehicle-form").hide(600);
                    $("#vehicle-select").show(600);
                }
            };

            var changeState = function(){
                $('.pace').show(); // Show loading
                $.ajax({
                    type: "GET",
                    url: '{!! url("api/getCities/'+getStateId()+'") !!}',
                    async: false,
                    success:function(cities){
                        $('.pace').hide();
                        $('#s2example-2').children('option').remove(); // Clean 
                        $.each(cities, function(id, city) {
                          
                            $('#s2example-2').append('<option value="' + id + '">' + city + '</option>');
                           
                        });
                        
                    },
                    error: function(){
                        $('.pace').hide();
                        alert("Houston!, we have a problem!");
                        $('#s2example-2').children('option').remove();
                    }
                });
            }
            

            var changeManufactures = function(){
                $('.pace').show(); // Show loading
                $.ajax({
                    type: "GET",
                    url: '{!! url("api/getModel/'+getManufacturerId()+'") !!}',
                    async: false,
                    success:function(vehicle__models){
                        $('.pace').hide();
                        $('#s2example-4').children('option').remove();
                        $.each(vehicle__models, function(id, Vehicle_Model) {
                            
                            $('#s2example-4').append('<option value="' + id + '">' + Vehicle_Model + '</option>');

                        });
                     
                    },
                    error: function(){
                        $('.pace').hide();
                        alert("Houston!, we have a problem!");
                        
                    }
                });
            }

            $('#driver_type_select').on('change', changeVehicleDriverForm);
            $('#s2example-1').on('change', changeState);
            $('#s2example-3').on('change', changeManufactures);
            

            // Execute when document is loaded
            changeVehicleDriverForm();
            changeState(); 
            changeManufactures();
            
        })
    </script>
 <script src="assets/plugins/inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script><script src="assets/plugins/autonumeric/autoNumeric.js" type="text/javascript"></script><!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END --> 
</body>

</html>
