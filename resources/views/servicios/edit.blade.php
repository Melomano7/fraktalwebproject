@extends('layouts.app')

	@section('title','Servicios Edit')

	@section('content')
	<form class="form-group" method="POST" action="/Servicio/{{$servicios->nombre}}"
     enctype="multipart/form-data"
	>
	
    {{ csrf_field() }}
    {{ method_field('PATCH') }}
	
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Nombre</label>
			<input type="text" class="form-control" name="nombre" value="{{$servicios->nombre}}">
			
		</div>
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Posicion</label>
			<input type="text" class="form-control" name="posicion" value="{{$servicios->posicion}}">
			
		</div>

		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Avatar</label>
			<input type="file" name="img_url">
			<br>
		</div>
		<br>
		<div class="text-center">
			<input class="btn btn-success bg-info" type="submit" value="Editar">
			
		</div>
			
		</form>
	@endsection
