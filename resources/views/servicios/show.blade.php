@extends('layouts.app')

@section('title','Servicios')

@section('content')
@include('common.success')
 <img class="card-img-top
	   rounded-circle mx-auto d-block" src="/images/{{$servicios->img_url}}" alt="" 
       style="height: 100px;width: 100px;background-color:#EFEFEF;margin: 30px"
	   >
	   <div class="text-center">
	   <h5 class="card-title">{{$servicios->nombre}}</h5>
	    <a href="/Servicios/{{$servicios->nombre}}" class="btn btn-primary">Editar</a>
	    <a href="/Servicios/{{$servicios->nombre}}" class="btn btn-danger">Eliminar</a>
	   </div>

@endsection