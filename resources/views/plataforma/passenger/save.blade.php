    
@extends('plataforma.layout')
@section('title','Pasajero')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

           <div class="col-lg-12">
             @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section class="box ">

                <header class="panel_header">
                    <h2 class="title pull-left">Guardar Pasajero</h2>
                    <div class="actions panel_actions pull-right">
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php $lectura = "disabled" ?>
                        @if($city->passenger_id)
                        <?php $lectura = "enabled" ?>
                        @endif

                        {{ Form::open( array('url' => 'plataforma/Passenger/'.$city->passenger_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}


                            <div class="form-group">
                                <label for="">Latitud*</label>
                                <input type="text" class="form-control" name="lat" value="{{$city->lat}}"></div>
                                <div class="form-group">
                                    <label for="">Longitud*</label>
                                    <input type="text" class="form-control" name="lng" value="{{$city->lng}}"></div>
                                      <div class="form-group">
                                <label for="">Estatus*</label>

                                <select class="form-control" name="status">  
                                    <option value="Activo">Activo</option>
                                    <option value="Inactivo">Inactivo</option>
                                </select>
                            </div>
                                      <div class="form-group">
                                <label for="">Tipo de pago*</label>
                                <select class="form-control" name="payment_type_id">
                                    @foreach($state as $sta)
                                    <option value="{{$sta->payment_type_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Usuario*</label>
                                <select class="form-control" name="user_id">
                                    @foreach($fku as $sta)
                                    <option value="{{$sta->user_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>


                                                    @if($city->passenger_id)
                                                    {{ Form::hidden ('_method', 'PUT', ['passenger_id' => 'methodo']) }}
                                                    @endif

                                                    <div class="text-right" style="margin-top:120px;">
                                                        {{ link_to('/plataforma/Passenger/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                                        {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'passenger_id' => 'guardar']) }}
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ Form::close() }}


                                                </div>
                                            </div>
                                        </div>


                                    </section>
                                    <br>
                                    <br>

                                </div>
                                @endsection