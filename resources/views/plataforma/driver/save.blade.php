    
      
@extends('plataforma.layout')
@section('title','Conductor')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

         <div class="col-lg-12">
           @if($errors->any())
           <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <section class="box ">

            <header class="panel_header">
                <h2 class="title pull-left">GUARDAR Conductor</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php $lectura = "disabled" ?>
                    @if($vehiculo->driver_id)
                    <?php $lectura = "enabled" ?>
                    @endif

                    {{ Form::open( array('url' => 'plataforma/Driver/'.$vehiculo->driver_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}
                               <?php
                               $nombre='';
                               $correo='';
                               $telefono='';
                               $bandera=0;
                                  ?>
                                @foreach($usuarios as $sta)
                                 @if($vehiculo->user_id==$sta->user_id)
                                 <?php $bandera=1; ?>
                                 <div class="form-group">
                            <label for="">Nombre*</label>
                            <input type="text" class="form-control" name="name" value="{{$sta->name}}"></div>
                            <div class="form-group">
                            <label for="">Correo*</label>
                            <input type="text" class="form-control" name="email" value="{{$sta->email}}"></div>

                              <div class="form-group">
                            <label for="">Telefono*</label>
                            <input type="text" class="form-control" name="phone" value="{{$sta->phone}}"></div>
                                                    @endif             

                                                    @endforeach

                             @if($bandera==0)
                             <div class="form-group">
                            <label for="">Nombre*</label>
                            <input type="text" class="form-control" name="name" value=""></div>

                            <div class="form-group">
                            <label for="">Correo*</label>
                            <input type="text" class="form-control" name="email" value=""></div>

                              <div class="form-group">
                            <label for="">Telefono*</label>
                            <input type="text" class="form-control" name="phone" value=""></div>
                             @endif
                            <div class="form-group">
                                <label for="">Vehiculo*</label>
                                <select class="form-control" name="vehicle_id">
                                    @foreach($vehicle as $sta)
                                    <option value="{{$sta->vehicle_id}}">{{$sta->title}}</option>
                                    @endforeach

                                </select>
                            </div>


                              <div class="form-group">
                            <label for="">Licencia de Manejo*</label>
                            <input type="text" class="form-control" name="driver_licence" value="{{$vehiculo->driver_licence}}"></div>

                             <div class="form-group">
                            <label for="">Fecha de vencimiento de la licencia*</label>
                            <input type="text" class="form-control datepicker" data-format="D, dd MM yyyy"  value="{{$vehiculo->license_expiry_date}}" name="license_expiry_date">
                            
                        </div>
                            
                           <div class="form-group">
                            <label for="">Cuenta bancaria*</label>
                            <input type="text" class="form-control" name="banck_account" value="{{$vehiculo->banck_account}}"></div>

                             <div class="form-group">
                            <label for="">Acerca de*</label>
                            <input type="text" class="form-control" name="about" value="{{$vehiculo->about}}"></div>
                             
                              <div class="form-group">
                                <label for="">Tipo de chofer*</label>
                                <select class="form-control" name="driver_type">
                                    
                                    <option value="Propietario">Propietario</option>
                                    <option value="Chofer">Chofer</option>
                                </select>
                            </div>
                           

                             <div class="form-group">
                            <label for="">Dirección del conductor*</label>
                            <input type="text" class="form-control" name="home_address" value="{{$vehiculo->home_address}}"></div>

                           
                          <div class="form-group">
                                <label for="">Estatus*</label>
                                <select class="form-control" name="status">
                                    
                                    <option value="active">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                </select>
                            </div>

                            <div class="form-group">
                            {{ Form::label ('imagen', 'Imagen *', ['class' => 'form-label']) }}
                            <div class="controls">
                                <i class=""></i>
                                @if($vehiculo->driver_id)
                                  {{ Form::file ('driver_licence_img_src', ['class' => 'custom-file-input', 'driver_id' => 'driver_licence_img_src']) }}
                                @else
                                  {{ Form::file ('driver_licence_img_src', ['class' => 'custom-file-input', 'driver_id' => 'driver_licence_img_src']) }}
                                @endif
                                @if($errors->first('driver_licence_img_src'))
                                  <div class="alert alert-error alert-dismissible fade in">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      {{ $errors->first('driver_licence_img_src') }}
                                  </div>
                                @endif
                            </div>
                            <p>Medidas sugeridas: 1220 * 790</p>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" style="top:-60px;">
                                {{ Html::image($vehiculo->driver_licence_img_src, '', array('class' => 'img-thumbnail img-responsive', 'alt' => '140x140', 'id' => 'imagenChange', 'data-holder-rendered' => 'true')) }}
                                <a class="preview previewGasto" href="/{{ $vehiculo->driver_licence_img_src }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>


                            @if($vehiculo->driver_id)
                            {{ Form::hidden ('_method', 'PUT', ['driver_id' => 'methodo']) }}
                            @endif

                            <div class="text-right" style="margin-top:120px;">
                                {{ link_to('/plataforma/Driver/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'driver_id' => 'guardar']) }}
                            </div>
                            {{ csrf_field() }}
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>


            </section>
            <br>
            <br>

        </div>
        @endsection