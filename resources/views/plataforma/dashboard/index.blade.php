
@extends('plataforma.layout')

@section('title','Dashboard')

@section('content')
<div class="row">

  <div class="col-md-3 col-sm-6 col-xs-6">
    <div class="r4_counter db_box">
      <i class="pull-left fa fa-thumbs-up icon-md icon-rounded icon-primary"></i>
      <div class="stats">
        <h4><strong>45%</strong></h4>
        <span>New Orders</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-6">
    <div class="r4_counter db_box">
      <i class="pull-left fa fa-shopping-cart icon-md icon-rounded icon-orange"></i>
      <div class="stats">
        <h4><strong>243</strong></h4>
        <span>Nuevos Servicios</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-6">
    <div class="r4_counter db_box">
      <i class="pull-left fa fa-dollar icon-md icon-rounded icon-purple"></i>
      <div class="stats">
        <h4><strong>$13424</strong></h4>
        <span>Ingresos del dia</span>
      </div>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 col-xs-6">
    <div class="r4_counter db_box">
      <i class="pull-left fa fa-users icon-md icon-rounded icon-warning"></i>
      <div class="stats">
        <h4><strong>133</strong></h4>
        <span>Nuevos Usuarios</span>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-5 col-sm-12 col-xs-12">
    <div class="r3_notification db_box">
      <h4>Notificaciones</h4>

      <ul class="list-unstyled notification-widget ps-container ps-active-y" style="height: 315px;">


        <li class="unread status-available">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Luis</strong>
                <span class="time small">- 15 mins ago</span>
                <span class="profile-status available pull-right"></span>
              </span>
              <span class="desc small">
                Buenos Precios.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-away">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Marco</strong>
                <span class="time small">- 45 mins ago</span>
                <span class="profile-status away pull-right"></span>
              </span>
              <span class="desc small">
                Espere mucho tiempo.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-busy">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Toño</strong>
                <span class="time small">- 1 hour ago</span>
                <span class="profile-status busy pull-right"></span>
              </span>
              <span class="desc small">
                A mejorado mucho el servicio.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-offline">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Paco</strong>
                <span class="time small">- 5 hours ago</span>
                <span class="profile-status offline pull-right"></span>
              </span>
              <span class="desc small">
                Las unidades son muy viejas.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-offline">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Alejandro</strong>
                <span class="time small">- Yesterday</span>
                <span class="profile-status offline pull-right"></span>
              </span>
              <span class="desc small">
                Exelente servicio.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-available">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Brenda</strong>
                <span class="time small">- 14th Mar</span>
                <span class="profile-status available pull-right"></span>
              </span>
              <span class="desc small">
                Me gusto mucho la rapidez y el costo del traslado.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-busy">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Angel</strong>
                <span class="time small">- 16th Mar</span>
                <span class="profile-status busy pull-right"></span>
              </span>
              <span class="desc small">
                El costo del servicio es muy elevado.
              </span>
            </div>
          </a>
        </li>


        <li class=" status-away">
          <a href="javascript:;">
            <div class="user-img">
              <img src="assets/images/user.png" alt="user-image" class="img-circle img-inline">
            </div>
            <div>
              <span class="name">
                <strong>Felipe</strong>
                <span class="time small">- 16th Mar</span>
                <span class="profile-status away pull-right"></span>
              </span>
              <span class="desc small">
                UBER tiene un mejor servicio y a mejor costo.
              </span>
            </div>
          </a>
        </li>


        <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 315px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 135px;"></div></div></ul>

      </div>
    </div>    

    <div class="col-md-3 col-sm-6 col-xs-12">
      <div class="r3_weather">
        <div class="wid-weather wid-weather-small">
          <div class="">

            <div class="location">
              <h3>Morelia, MEX</h3>
              <span>Hoy, 26</sup> de Mayo 2019</span>
            </div>
            <div class="clearfix"></div>
            <div class="degree">
              <i class="fa fa-cloud icon-lg text-white"></i><span>Ahora</span><h3>24°</h3>
              <div class="clearfix"></div>
              <h4 class="text-white text-center"></h4>
            </div>
            <div class="clearfix"></div>
            <div class="weekdays bg-white">
              <ul class="list-unstyled ps-container ps-active-y">
                <li><span class="day">Lunes</span><i class="fa fa-cloud icon-xs"></i><span class="temp">23° - 27°</span></li>
                <li><span class="day">Martes</span><i class="fa fa-cloud icon-xs"></i><span class="temp">21° - 26°</span></li>
                <li><span class="day">Miercoles</span><i class="fa fa-cloud icon-xs"></i><span class="temp">24° - 28°</span></li>
                <li><span class="day">Jueves</span><i class="fa fa-cloud icon-xs"></i><span class="temp">25° - 26°</span></li>
                <li><span class="day">Viernes</span><i class="fa fa-cloud icon-xs"></i><span class="temp">22° - 25°</span></li>
                <li><span class="day">Sabado</span><i class="fa fa-cloud icon-xs"></i><span class="temp">21° - 28°</span></li>
                <li><span class="day">Domingo</span><i class="fa fa-cloud icon-xs"></i><span class="temp">23° - 29°</span></li>
                <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; height: 175px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 99px;"></div></div></ul>
              </div>

            </div>
          </div>

        </div>
      </div>    

      <div class="col-md-4 col-sm-6 col-xs-12">

        <div class="ultra-widget ultra-todo-task bg-primary">
          <div class="wid-task-header">
            <div class="wid-icon">
              <i class="fa fa-tasks"></i>
            </div>
            <div class="wid-text">
              <h4>Tareas Pendientes</h4>
              <span>Dom, <small>26<sup></sup> de Mayo 2019</small></span>
            </div>
          </div>
          <div class="wid-all-tasks">

            <ul class="list-unstyled ps-container">

              <li class="checked">
                <div class="icheckbox_minimal-white checked" style="position: relative;"><input type="checkbox" id="task-1" class="icheck-minimal-white todo-task" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                <label class="icheck-label form-label" for="task-1">Revisar Notificaciones</label>
              </li> 
              <li>
                <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-2" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                <label class="icheck-label form-label" for="task-2">Generar Reportes de Ingresos</label>
              </li>  

              <li>
                <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-3" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                <label class="icheck-label form-label" for="task-3">Monitoriar Usuarios</label>
              </li> 
              <li>
                <div class="icheckbox_minimal-white" style="position: relative;"><input type="checkbox" id="task-4" class="icheck-minimal-white todo-task" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;"></ins></div>
                <label class="icheck-label form-label" for="task-4">Monitoriar Choferes</label>
              </li> 
              <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;"><div class="ps-scrollbar-x" style="left: 0px; width: 0px;"></div></div><div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px;"><div class="ps-scrollbar-y" style="top: 0px; height: 0px;"></div></div></ul>

            </div>
            <div class="wid-add-task">
              <input type="text" class="form-control" placeholder="Agregar mas tareas">
            </div>
          </div>


        </div>    

      </div>
    </section></div>
    @endsection