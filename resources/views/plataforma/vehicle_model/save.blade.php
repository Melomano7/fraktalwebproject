    
@extends('plataforma.layout')
@section('title','Modelos')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

         <div class="col-lg-12">
           @if($errors->any())
           <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <section class="box ">

            <header class="panel_header">
                <h2 class="title pull-left">GUARDAR Modelo</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php $lectura = "disabled" ?>
                    @if($state->vehicle_model_id)
                    <?php $lectura = "enabled" ?>
                    @endif

                    {{ Form::open( array('url' => 'plataforma/Vehicle_Model/'.$state->vehicle_model_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                    <div class="form-group">
                        <label for="">Nombre*</label>
                        <input type="text" class="form-control" name="name" value="{{$state->name}}"></div>

                             <div class="form-group">
                                <label for="">Fabricante*</label>
                                <select class="form-control" name="manufacturer_id">
                                    @foreach($fabricante as $sta)
                                    <option value="{{$sta->manufacturer_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                            @if($state->vehicle_model_id)
                            {{ Form::hidden ('_method', 'PUT', ['vehicle_model_id' => 'methodo']) }}
                            @endif

                            <div class="text-right" style="margin-top:120px;">
                                {{ link_to('/plataforma/Vehicle_Model/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'vehicle_model_id' => 'guardar']) }}
                            </div>
                            {{ csrf_field() }}
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>


            </section>
            <br>
            <br>

        </div>
        @endsection