    
@extends('plataforma.layout')
@section('title','Tipo de vehiculo')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

         <div class="col-lg-12">
           @if($errors->any())
           <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <section class="box ">

            <header class="panel_header">
                <h2 class="title pull-left">GUARDAR Tipo de Vehiculo</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php $lectura = "disabled" ?>
                    @if($state->vehicle_type_id)
                    <?php $lectura = "enabled" ?>
                    @endif

                    {{ Form::open( array('url' => 'plataforma/Vehicle_Type/'.$state->vehicle_type_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                    <div class="form-group">
                        <label for="">Nombre*</label>
                        <input type="text" class="form-control" name="name" value="{{$state->name}}"></div>
                    <div class="form-group">
                        <label for="">Numero maximo de personas*</label>
                        <input type="text" class="form-control" name="max_persons" value="{{$state->max_persons}}"></div>
                      <div class="form-group">
                        <label for="">Tarifa base*</label>
                        <input type="text" class="form-control" name="base_fare" value="{{$state->base_fare}}"></div>
                      <div class="form-group">
                          <label for="">Tarifa minima*</label>
                        <input type="text" class="form-control" name="min_fare" value="{{$state->min_fare}}"></div>
                      <div class="form-group">
                         <label for="">Precio por minuto*</label>
                        <input type="text" class="form-control" name="price_per_min" value="{{$state->price_per_min}}"></div>
                       <div class="form-group">

                         <label for="">Precio por cancelacion*</label>
                        <input type="text" class="form-control" name="cancellation_price" value="{{$state->cancellation_price}}"></div>

                         <div class="form-group">
                                <label for="">Ciudad*</label>
                                <select class="form-control" name="city_id">
                                    @foreach($city as $sta)
                                    <option value="{{$sta->city_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                        
                            
                        <div class="form-group">
                         <label for="">Precio minimo por distancia*</label>
                        <input type="text" class="form-control" name="min_price_per_distance" value="{{$state->min_price_per_distance}}"></div>
                       <div class="form-group">
                        <label for="">Precio maximo por distancia*</label>
                        <input type="text" class="form-control" name="max_price_per_distance" value="{{$state->max_price_per_distance}}"></div>
                          <div class="form-group">
                          <label for="">Tarifa base por minuto*</label>
                        <input type="text" class="form-control" name="base_fare_per_minute" value="{{$state->base_fare_per_minute}}"></div>
                        <div class="form-group">
                            {{ Form::label ('imagen', 'Imagen *', ['class' => 'form-label']) }}
                            <div class="controls">
                                <i class=""></i>
                                @if($state->vehicle_type_id)
                                  {{ Form::file ('img_src', ['class' => 'custom-file-input', 'vehicle_type_id' => 'img_src']) }}
                                @else
                                  {{ Form::file ('img_src', ['class' => 'custom-file-input', 'vehicle_type_id' => 'img_src', 'required' =>'required']) }}
                                @endif
                                @if($errors->first('img_src'))
                                  <div class="alert alert-error alert-dismissible fade in">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      {{ $errors->first('img_src') }}
                                  </div>
                                @endif
                            </div>
                            <p>Medidas sugeridas: 1220 * 790</p>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" style="top:-60px;">
                                {{ Html::image($state->img_src, '', array('class' => 'img-thumbnail img-responsive', 'alt' => '140x140', 'id' => 'imagenChange', 'data-holder-rendered' => 'true')) }}
                                <a class="preview previewGasto" href="/{{ $state->img_src }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>
                        
                            @if($state->vehicle_type_id)
                            {{ Form::hidden ('_method', 'PUT', ['vehicle_type_id' => 'methodo']) }}
                            @endif

                            <div class="text-right" style="margin-top:120px;">
                                {{ link_to('/plataforma/Vehicle_Type/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'vehicle_type_id' => 'guardar']) }}
                            </div>
                            {{ csrf_field() }}
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>


            </section>
            <br>
            <br>

        </div>
        @endsection