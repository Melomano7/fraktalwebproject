
     @extends('plataforma.layout')

    @section('title','Tipo de Vehiculo')

    @section('content')
    <div class="row">
        <div class="col-md-12">
             @if(Session::has('notice'))
              <div class="alert alert-error alert-dismissible fade in">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>{{ Session::get('notice') }}</strong>
              </div>
            @endif

            @if(Session::has('success'))
              <div class="alert alert-success alert-dismissible fade in">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>{{ Session::get('success') }}</strong>
              </div>
            @endif
        <section class="box ">
        
         
          
 <div class="col-lg-12">

                        <section class="box ">

                            <header class="panel_header">
                                <h2 class="title pull-left">Tipo de Vehiculo</h2>
                                <div class="actions panel_actions pull-right">
                  {{ Html::link('plataforma/Vehicle_Type/create', 'Crear Nuevo', array('class' => 'btn btn-info')) }}
              </div>
                            </header>
                            <div class="content-body">    <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">


                                        <table id="example-1" class="table table-striped dt-responsive display" cellspacing="0" width="100%">
                                            <thead>
                                                <tr>
                                                    <th>Nombre</th>
                                                    <th>Numero maximo de personas</th>
                                                    <th>Tarifa base</th>
                                                    <th>Ciudad</th>
                                                    <th>Precio por minuto</th>
                                                    
                                                  
                                                    
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>

                                           

                                            <tbody>
                                                @foreach($us as $user)
                                                <tr>
                                                    <td>{{$user->name}}</td>
                                                    <td>{{$user->max_persons}}</td>
                                                    <td>{{$user->base_fare}}</td>
                                                    <td>
                                                    @foreach($city as $sta)
                                                    @if($sta->city_id==$user->city_id)
                                                     {{$sta->name}}
                                                    @endif
                                                    @endforeach
                                                    </td>
                                                    <td>{{$user->price_per_min}}</td>
                                                   
                                                    
                                                    <td>
                                                    
                                        <a href="/plataforma/Vehicle_Type/{{$user->vehicle_type_id}}/edit" class="btn btn-info btn-xs pull-left right15" rel="tooltip" data-animate=" animated bounce" data-toggle="tooltip" data-original-title="Editar registro" data-placement="top">
                                          <i class="fa fa-pencil" ></i>
                                        </a>
                                        {{ Form::open(array('url' => '/plataforma/Vehicle_Type/'.$user->vehicle_type_id)) }}
                                        {{ Form::hidden("_method", "DELETE") }}
                                        {{ Form::submit("x", array('class' => 'btn btn-xs btn-danger pull-left right15', 'onclick' => 'return confirm("Seguro que deseas eliminar?");')) }}
                                        {{ Form::close() }}
                                    </td>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            
                           
                        </section>
                            <br>
                            <br>
                            
                      </div>
                        @endsection