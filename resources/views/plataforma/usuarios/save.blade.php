@extends('plataforma.layout')
@section('title','Usuarios')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

         <div class="col-lg-12">
           @if($errors->any())
           <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
        @endif
        <section class="box ">

            <header class="panel_header">
                <h2 class="title pull-left">GUARDAR USUARIO</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                    {{ Form::open( array('url' => 'plataforma/Usuarios/'.$user->user_id, 'method' => 'POST','id' => 'icon_validate', 'class' => '', 'enctype' => 'multipart/form-data') ) }}

                        <div class="form-group">
                            <label for="">Nombre*</label>
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                        </div>
                            
                        <div class="form-group">
                            <label for="">Correo*</label>
                            <input type="text" class="form-control" name="email" value="{{$user->email}}">
                        </div>

                        <div class="form-group">
                            <label for="">Rol*</label>
                            <select class="form-control" name="rol_id">
                                @foreach($rol as $roles)
                                    <option value="{{$roles->rol_id}}">{{$roles->name}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label for="">Contraseña*</label>
                            <input type="password" class="form-control" name="password" value="">
                        </div>

                        @if($user->user_id)
                        {{ Form::hidden ('_method', 'PUT', ['user_id' => 'methodo']) }}
                        @endif

                        <div class="text-right" style="margin-top:120px;">
                            {{ link_to('plataforma/Usuarios/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                            {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'user_id' => 'guardar']) }}
                        </div>

                        {{ csrf_field() }}

                    {{ Form::close() }}

                    </div>
                </div>
            </div>


        </section>
        <br>
        <br>

    </div>
@endsection