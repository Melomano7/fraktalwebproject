    
@extends('plataforma.layout')
@section('title','Empresa')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

           <div class="col-lg-12">
             @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section class="box ">

                <header class="panel_header">
                    <h2 class="title pull-left">Guardar Empresa</h2>
                    <div class="actions panel_actions pull-right">
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php $lectura = "disabled" ?>
                        @if($city->organizations_id)
                        <?php $lectura = "enabled" ?>
                        @endif

                        {{ Form::open( array('url' => 'plataforma/Organization/'.$city->organizations_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                        <div class="form-group">
                            <label for="">Nombre*</label>
                            <input type="text" class="form-control" name="name" value="{{$city->name}}"></div>
                        
                                 <div class="form-group">
                                <label for="">RFC*</label>
                                <input type="text" class="form-control" name="rfc" value="{{$city->rfc}}"></div>
                                <div class="form-group">
                                <label for="">Dirección*</label>
                                <input type="text" class="form-control" name="address" value="{{$city->address}}"></div>
                            <div class="form-group">
                                <label for="">Ciudad*</label>
                                <select class="form-control" name="city_id">
                                    @foreach($state as $sta)
                                    <option value="{{$sta->city_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Código Postal*</label>
                                <input type="text" class="form-control" name="cp" value="{{$city->cp}}"></div>
                                
                                  <div class="form-group">
                            <label for="">Representante de la compañia*</label>
                            <input type="text" class="form-control" name="company_representative" value="{{$city->company_representative}}"></div>


                             <div class="form-group">
                            <label for="">Cargo*</label>
                            <input type="text" class="form-control" name="position_company_representative" value="{{$city->position_company_representative}}"></div>
                                      
                            


                                                    @if($city->organizations_id)
                                                    {{ Form::hidden ('_method', 'PUT', ['organizations_id' => 'methodo']) }}
                                                    @endif

                                                    <div class="text-right" style="margin-top:120px;">
                                                        {{ link_to('/plataforma/Organization/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                                        {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'organizations_id' => 'guardar']) }}
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ Form::close() }}


                                                </div>
                                            </div>
                                        </div>


                                    </section>
                                    <br>
                                    <br>

                                </div>
                                @endsection