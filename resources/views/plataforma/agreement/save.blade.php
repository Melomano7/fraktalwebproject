    
    
@extends('plataforma.layout')
@section('title','Convenio')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

           <div class="col-lg-12">
             @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section class="box ">

                <header class="panel_header">
                    <h2 class="title pull-left">GUARDAR Convenio</h2>
                    <div class="actions panel_actions pull-right">
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php $lectura = "disabled" ?>
                        @if($agree->agreement_id)
                        <?php $lectura = "enabled" ?>
                        @endif

                        {{ Form::open( array('url' => 'plataforma/Agreement/'.$agree->agreement_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}
                         
                            <div class="form-group">
                                <label for="">Empresa*</label>
                                <select class="form-control" name="organizations_id">
                                    @foreach($organizacion as $sta)
                                    <option value="{{$sta->organizations_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>


                             <div class="form-group">
                            <label for="">Fecha de Terminación del Convenio*</label>
                            <input type="text" class="form-control datepicker" data-format="D, dd MM yyyy"  value="{{$agree->agreement_termination_date}}" name="agreement_termination_date">
                            
                        </div>


                              <div class="form-group">
                            <label for="">Vehiculos por semana*</label>
                            <input type="text" class="form-control" name="vehicles_per_week" value="{{$agree->vehicles_per_week}}"></div>

                              <div class="form-group">
                            <label for="">Descuento por viaje*</label>
                            <input type="text" class="form-control" name="discount_per_travel" value="{{$agree->discount_per_travel}}"></div>
                             


                                                    @if($agree->agreement_id)
                                                    {{ Form::hidden ('_method', 'PUT', ['agreement_id' => 'methodo']) }}
                                                    @endif

                                                    <div class="text-right" style="margin-top:120px;">
                                                        {{ link_to('/plataforma/Agreement/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                                        {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'agreement_id' => 'guardar']) }}
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ Form::close() }}


                                                </div>
                                            </div>
                                        </div>


                                    </section>
                                    <br>
                                    <br>

                                </div>
                                @endsection