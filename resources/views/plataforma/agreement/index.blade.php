
    
     @extends('plataforma.layout')

    @section('title','Convenios')

    @section('content')
    <div class="row">
        <div class="col-md-12">
             @if(Session::has('notice'))
              <div class="alert alert-error alert-dismissible fade in">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>{{ Session::get('notice') }}</strong>
              </div>
            @endif

            @if(Session::has('success'))
              <div class="alert alert-success alert-dismissible fade in">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                  <strong>{{ Session::get('success') }}</strong>
              </div>
            @endif
        <section class="box ">
        
         
          
 <div class="col-lg-12">

                        <section class="box ">

                            <header class="panel_header">
                                <h2 class="title pull-left">Convenios</h2>
                                <div class="actions panel_actions pull-right">
                  {{ Html::link('plataforma/Agreement/create', 'Crear Nuevo', array('class' => 'btn btn-info')) }}
              </div>
                            </header>
                            <div class="content-body">    <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">


                                        <table id="example-1" class="table table-striped dt-responsive display" cellspacing="0" width="100%">
                                          <thead>
                                                <tr>
                                                    <th>Empresa</th>
                                                    <th>Fecha de Terminación del Convenio</th>
                                                    <th>Numero de vehiculos por semana</th>
                                                      <th>Descuento por viaje</th>
                                                      discount_per_travel
                                                    <th>Opciones</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach($us as $user)
                                                <tr>
                                                     <td>
                                                    @foreach($nState as $sta)
                                                    @if($sta->organizations_id==$user->organizations_id)
                                                     {{$sta->name}}
                                                    @endif
                                                    @endforeach
                                                    </td>

                                                    <td>{{$user->agreement_termination_date}}</td>
                                                     
                                                     <td>{{$user->vehicles_per_week}}</td>
                                                     
                                                        <td>{{$user->discount_per_travel}}</td>
                                                      <td>

                                                    
                                        <a href="Agreement/{{$user->agreement_id}}/edit" class="btn btn-info btn-xs pull-left right15" rel="tooltip" data-animate=" animated bounce" data-toggle="tooltip" data-original-title="Editar registro" data-placement="top">
                                          <i class="fa fa-pencil" ></i>
                                        </a>
                                        {{ Form::open(array('url' => 'plataforma/Agreement/'.$user->agreement_id)) }}
                                        {{ Form::hidden("_method", "DELETE") }}
                                        {{ Form::submit("x", array('class' => 'btn btn-xs btn-danger pull-left right15', 'onclick' => 'return confirm("Seguro que deseas eliminar?");')) }}
                                        {{ Form::close() }}
                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>

                                    </div>
                                </div>
                            </div>
                            
                           
                        </section>
                            <br>
                            <br>
                            
                      </div>
                        @endsection