    
@extends('plataforma.layout')
@section('title','States')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

         <div class="col-lg-12">
           @if($errors->any())
           <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <section class="box ">

            <header class="panel_header">
                <h2 class="title pull-left">GUARDAR State</h2>
                <div class="actions panel_actions pull-right">
                </div>
            </header>
            <div class="content-body">    <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">

                    <?php $lectura = "disabled" ?>
                    @if($state->state_id)
                    <?php $lectura = "enabled" ?>
                    @endif

                    {{ Form::open( array('url' => 'plataforma/States/'.$state->state_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                    <div class="form-group">
                        <label for="">Nombre*</label>
                        <input type="text" class="form-control" name="name" value="{{$state->name}}"></div>

                        <div class="form-group">
                            <label for="">Nombre abreviado*</label>
                            <input type="text" class="form-control" name="short_name" value="{{$state->short_name}}"></div>


                            @if($state->state_id)
                            {{ Form::hidden ('_method', 'PUT', ['state_id' => 'methodo']) }}
                            @endif

                            <div class="text-right" style="margin-top:120px;">
                                {{ link_to('/plataforma/States/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'state_id' => 'guardar']) }}
                            </div>
                            {{ csrf_field() }}
                            {{ Form::close() }}


                        </div>
                    </div>
                </div>


            </section>
            <br>
            <br>

        </div>
        @endsection