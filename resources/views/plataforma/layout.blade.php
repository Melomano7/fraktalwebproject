<!DOCTYPE html>
<html class=" ">

<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/logo-folded.png')}}">
    <title>@yield('title', 'Plataforma : Administración')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="Laravel boilerplate" name="description" />
    <meta content="Fraktalweb" name="author" />

    <!-- CORE CSS FRAMEWORK - START -->
    {{Html::style('assets/plugins/pace/pace-theme-flash.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/bootstrap/css/bootstrap.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/bootstrap/css/bootstrap-theme.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/fonts/font-awesome/css/font-awesome.css', array('media'=>'screen'))}}
    {{Html::style('assets/css/animate.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/perfect-scrollbar/perfect-scrollbar.css', array('media'=>'screen'))}}
    <!-- CORE CSS FRAMEWORK - END -->

    {{Html::style('assets/plugins/jquery-ui/smoothness/jquery-ui.min.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/datepicker/css/datepicker.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/daterangepicker/css/daterangepicker-bs3.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/daterangepicker/css/daterangepicker-bs3.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datetimepicker/css/datetimepicker.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/timepicker/css/timepicker.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datatables/css/jquery.dataTables.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datatables/extensions/TableTools/css/dataTables.tableTools.min.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.css', array('media'=>'screen'))}}
    {{Html::style('assets/plugins/select2/select2.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/bootstrap3-wysihtml5/css/bootstrap3-wysihtml5.min.css', array('media' => 'screen'))}}
    {{Html::style('assets/plugins/messenger/css/messenger.css')}}
    {{Html::style('assets/plugins/messenger/css/messenger-theme-future.css')}}
    {{Html::style('assets/plugins/messenger/css/messenger-theme-flat.css')}}
    {{Html::style('assets/plugins/icheck/skins/all.css', array('media'=>'screen')) }}
    {{Html::style('assets/plugins/multi-select/css/multi-select.css', array('media' => 'screen')) }}
    {{Html::style('assets/plugins/prettyphoto/prettyPhoto.css', array('media' => 'screen')) }}
    {{Html::style('assets/plugins/tagsinput/css/bootstrap-tagsinput.css', array('media' => 'screen')) }}




      <!-- CORE CSS TEMPLATE - START -->
      {{Html::style('assets/css/style.css', array('media'=>'screen'))}}
      {{Html::style('assets/css/responsive.css', array('media'=>'screen'))}}
      {{Html::style('css/main.css', array('media'=>'screen'))}}
      <!-- CORE CSS TEMPLATE - END -->

    @yield('head')

</head>
<!-- END HEAD -->

<!-- BEGIN BODY -->
<body class=" "><!-- START TOPBAR -->

<div class='page-topbar'>
    <div class='logo-area'>
    </div>
    <div class='quick-area'>
        <div class='pull-left'>
            <ul class="info-menu left-links list-inline list-unstyled">

                

                <li class="sidebar-toggle-wrap">
                    <a href="#" data-toggle="sidebar" class="sidebar_toggle">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class='pull-right'>
            <ul class="info-menu right-links list-inline list-unstyled">
                <li class="profile">
                    <a href="#" data-toggle="dropdown" class="toggle">
                        @if(Auth::check() && strlen(Auth::user()->img_src)>5)
                          <img src="/{{Auth::user()->img_src}}" alt="user-image" class="img-circle img-inline">
                        @else
                          <img src="{{URL::asset('assets/images/user.png')}}" alt="user-image" class="img-circle img-inline">
                        @endif
                        <!-- <img src="{{URL::asset('img/profile.jpg')}}" alt="user-image" class="img-circle img-inline"> -->
                        <span>
                          @if(Auth::check())
                            {{ Auth::user()->name }}
                          @else
                            Administrador
                          @endif
                         <i class="fa fa-angle-down"></i></span>
                    </a>
                    <ul class="dropdown-menu profile animated fadeIn">
                      @if(Auth::check())
                        <li>
                              <a href="/plataforma/Usuarios/{{Auth::user()->user_id}}/edit">
                              <i class="fa fa-user"></i>
                              Editar Perfil
                            </a>
                        </li>
                      @endif
                        <!-- <li>
                            <a href="#help">
                                <i class="fa fa-info"></i>
                                Ayuda
                            </a>
                        </li> -->
                        <li class="last">
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                                <i class="fa fa-lock"></i>
                                Salir
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>

</div>
<!-- END TOPBAR -->
<!-- START CONTAINER -->

<div class="page-container row-fluid"  >

    <!-- SIDEBAR - START -->
    <div class="page-sidebar" style="height:100%;">


        <!-- MAIN MENU - START -->
        <div class="page-sidebar-wrapper main-menu-wrapper" id="main-menu-wrapper">

      
            <ul class='wraplist'>


                <li class="{{ (Request::is('plataforma') ? 'open' : '') }}">
                    <a href=" {{ url('plataforma') }}">
                        <i class="fa fa-dashboard"></i>
                        <span class="title">{{ trans('Dashboard') }}</span>
                    </a>
                </li>

                <li class="{{ (Request::is('plataforma/Usuarios') ? 'open' : '') }}">
                   
                    <a href=" {{ url('plataforma/Usuarios') }}">
                        <i class="fa fa-group"></i>
                        <span class="title">{{ trans('Usuarios') }}</span>
                    </a>
                </li>

                <li class="{{ ((Request::is('plataforma/Vehicle*') || Request::is('plataforma/Manufacturers*') ) ? 'open' : '') }}">
                    <a href="javascript:;">
                        <i class="fa fa-cab"></i>
                        <span class="title">Vehiculos</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" >
                         <li>
                            <a class="{{ ((Request::is('plataforma/Vehicle') || Request::is('plataforma/Vehicle/*')) ? 'active' : '') }}" href="{{ url('plataforma/Vehicle') }}">Vehiculos</a>
                        </li>
                        <li>
                            <a class="{{ (Request::is('plataforma/Vehicle_Type*') ? 'active' : '') }}" href="{{ url('plataforma/Vehicle_Type') }}">Tipos de Vehiculo</a>
                        </li>
                       <li>
                            <a class="{{ (Request::is('plataforma/Vehicle_Model*') ? 'active' : '') }}" href="{{ url('plataforma/Vehicle_Model') }}">Modelos</a>
                        </li>
                        <li>
                            <a class="{{ (Request::is('plataforma/Manufacturers*') ? 'active' : '') }}" href="{{ url('plataforma/Manufacturers') }}">Fabricantes</a>
                        </li>
                    </ul>
                </li>
                 
                <li class="{{ (Request::is('plataforma/States') ? 'open' : '') }}">
                    <a href="{{ url('plataforma/States') }}">
                        <i class="glyphicon glyphicon-road"></i>
                        <span class="title">{{ trans('Estados') }}</span>
                    </a>
                </li>

               
                <li class="{{ (Request::is('plataforma/Cities') ? 'open' : '') }}">
                    <a href=" {{ url('plataforma/Cities') }}">
                        <i class="fa fa-map-marker"></i>
                        <span class="title">{{ trans('Ciudades') }}</span>
                    </a>
                </li>

                <li class="{{ (Request::is('plataforma/Payment_Type') ? 'open' : '') }}">
                    <a href=" {{ url('plataforma/Payment_Type') }}">
                        <i class="fa fa-credit-card"></i>
                        <span class="title">{{ trans('Tipo de Pago') }}</span>
                    </a>
                </li>
                
                       
                <li class="{{ (Request::is('plataforma/Passenger') ? 'open' : '') }}">
                    <a href=" {{ url('plataforma/Passenger') }}">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="title">{{ trans('Pasajeros') }}</span>
                    </a>
                </li>
                <li class="{{ (Request::is('plataforma/Driver') ? 'open' : '') }}">
                    <a href="{{ url('plataforma/Driver') }}">
                        <i class="glyphicon glyphicon-user"></i>
                        <span class="title">{{ trans('Conductores') }}</span>
                    </a>
                </li>
                 <li class="{{ ((Request::is('plataforma/Organization*') || Request::is('plataforma/Organization*') ) ? 'open' : '') }}">
                    <a href="javascript:;">
                        <i class="fa fa-building-o"></i>
                        <span class="title">Empresas</span>
                        <span class="arrow"></span>
                    </a>
                    <ul class="sub-menu" >
                         <li>
                            <a class="{{ ((Request::is('plataforma/Organization') || Request::is('plataforma/Organization/*')) ? 'active' : '') }}" href="{{ url('plataforma/Organization') }}">Empresas</a>
                        </li>
                        <li>
                            <a class="{{ (Request::is('plataforma/Agreement*') ? 'active' : '') }}" href="{{ url('plataforma/Agreement') }}">Convenios</a>
                        </li>
                       
                    </ul>
                </li>
                

            </ul>

        </div>

        <!-- MAIN MENU - END -->
  
                

        <div class="project-info text-center" style="">
          <p> Desarrollado por Fraktalweb</p>
        </div>



    </div>
    <!--  SIDEBAR - END -->
    <!-- START CONTENT -->
    <section id="main-content" class="">


        <section class="wrapper" style='margin-top:60px;display:inline-block;width:100%;'>

            <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                <div class="page-title">

                    <div class="pull-left">
                        <h1 class="title">@yield('main_title')</h1>
                    </div>


                </div>
            </div>
            <div class="clearfix"></div>


            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="width: 100% !important; margin-left: 0px !important; margin-right: 0px !important; padding-left: 0px !important; padding-right: 0px !important;">


                @yield('content')

            </div>



        </section>
    </section>
    <!-- END CONTENT -->


  </div>
<!-- END CONTAINER -->
<!-- LOAD FILES AT PAGE END FOR FASTER LOADING -->

@section('footer')

  <!-- CORE JS FRAMEWORK - START -->
  {{ HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js') }}
  <!-- {{-- HTML::script('assets/js/jquery-1.11.2.min.js') --}} -->
  {{ HTML::script('assets/js/jquery.easing.min.js')}}
  {{ HTML::script('assets/plugins/bootstrap/js/bootstrap.min.js')}}
  {{ HTML::script('assets/plugins/pace/pace.min.js')}}
  {{ HTML::script('assets/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}
  {{ HTML::script('assets/plugins/viewport/viewportchecker.js') }}
  <!-- CORE JS FRAMEWORK - END -->

  <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - START -->
  {{ HTML::script('assets/plugins/datepicker/js/datepicker.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/daterangepicker/js/moment.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/daterangepicker/js/daterangepicker.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/timepicker/js/timepicker.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datetimepicker/js/datetimepicker.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/jquery-validation/js/jquery.validate.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/jquery-validation/js/additional-methods.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/js/form-validation.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datatables/js/jquery.dataTables.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/datatables/extensions/Responsive/bootstrap/3/dataTables.bootstrap.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/bootstrap3-wysihtml5/js/bootstrap3-wysihtml5.all.min.js', ["type" => "text/javascript"])}}
  {{ Html::script('assets/plugins/jquery-ui/smoothness/jquery-ui.min.js', ["type" => "text/javascript"]) }}
  {{ Html::script('assets/plugins/icheck/icheck.min.js', ["type" => "text/javascript"]) }}
  {{ Html::script('assets/plugins/select2/select2.min.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/multi-select/js/jquery.multi-select.js', ["type" => "text/javascript"])}}
  {{ HTML::script('assets/plugins/multi-select/js/jquery.quicksearch.js', ["type" => "text/javascript"]) }}
  {{ HTML::script('assets/plugins/prettyphoto/jquery.prettyPhoto.js', ["type" => "text/javascript"]) }}

  <!-- OTHER SCRIPTS INCLUDED ON THIS PAGE - END -->


  <!-- CORE TEMPLATE JS - START -->
  {{ HTML::script('assets/js/scripts.js') }}
  <!-- END CORE TEMPLATE JS - END -->



@show

@section('moreJs')

@show

<script type="text/javascript">


</script>

</body>
</html>
