    
@extends('plataforma.layout')
@section('title','Ciudad')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

           <div class="col-lg-12">
             @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section class="box ">

                <header class="panel_header">
                    <h2 class="title pull-left">Guardar Ciudad</h2>
                    <div class="actions panel_actions pull-right">
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php $lectura = "disabled" ?>
                        @if($city->city_id)
                        <?php $lectura = "enabled" ?>
                        @endif

                        {{ Form::open( array('url' => 'plataforma/Cities/'.$city->city_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                        <div class="form-group">
                            <label for="">Nombre*</label>
                            <input type="text" class="form-control" name="name" value="{{$city->name}}"></div>

                            <div class="form-group">
                                <label for="">Estado*</label>
                                <select class="form-control" name="state_id">
                                    @foreach($state as $sta)
                                    <option value="{{$sta->state_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Latitud*</label>
                                <input type="text" class="form-control" name="lat" value="{{$city->lat}}"></div>
                                <div class="form-group">
                                    <label for="">Longitud*</label>
                                    <input type="text" class="form-control" name="ing" value="{{$city->ing}}"></div>
                                    <div class="form-group">
                                        <label for="">Rango de Kilometros*</label>
                                        <input type="text" class="form-control" name="km_range" value="{{$city->km_range}}"></div>
                                      
                            <div class="form-group">
                                <label for="">Estatus*</label>
                                <select class="form-control" name="status">
                                    
                                    <option value="active">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                </select>
                            </div>


                                                    @if($city->city_id)
                                                    {{ Form::hidden ('_method', 'PUT', ['city_id' => 'methodo']) }}
                                                    @endif

                                                    <div class="text-right" style="margin-top:120px;">
                                                        {{ link_to('/plataforma/Cities/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                                        {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'city_id' => 'guardar']) }}
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ Form::close() }}


                                                </div>
                                            </div>
                                        </div>


                                    </section>
                                    <br>
                                    <br>

                                </div>
                                @endsection