    
    
@extends('plataforma.layout')
@section('title','Vehiculos')

@section('content')
<div class="row">
    <div class="col-md-12">
        <section class="box ">

           <div class="col-lg-12">
             @if($errors->any())
             <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <section class="box ">

                <header class="panel_header">
                    <h2 class="title pull-left">GUARDAR Vehiculo</h2>
                    <div class="actions panel_actions pull-right">
                    </div>
                </header>
                <div class="content-body">    <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <?php $lectura = "disabled" ?>
                        @if($vehiculo->vehicle_id)
                        <?php $lectura = "enabled" ?>
                        @endif

                        {{ Form::open( array('url' => 'plataforma/Vehicle/'.$vehiculo->vehicle_id, 'method' => 'POST','id' => 'icon_validate', 'class' => 'datosPedido', 'enctype' => 'multipart/form-data') ) }}

                        <div class="form-group">
                            <label for="">Titulo*</label>
                            <input type="text" class="form-control" name="title" value="{{$vehiculo->title}}"></div>

                         <div class="form-group">
                                <label for="">Conductor*</label>
                                <select class="form-control" name="driver_id">
                                    @foreach($con as $sta)
                                    <option value="{{$sta->driver_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>
                         <div class="form-group">
                                <label for="">Estatus*</label>
                                <select class="form-control" name="status">
                                    
                                    <option value="active">Activo</option>
                                    <option value="inactive">Inactivo</option>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="">Modelo*</label>
                                <select class="form-control" name="vehicle_model_id">
                                    @foreach($modelo as $sta)
                                    <option value="{{$sta->vehicle_model_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Fabricante*</label>
                                <select class="form-control" name="manufacturer_id">
                                    @foreach($fabricante as $sta)
                                    <option value="{{$sta->manufacturer_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>


                             <div class="form-group">
                            <label for="">Color*</label>
                            <input type="text" class="form-control" name="color" value="{{$vehiculo->color}}"></div>


                             <div class="form-group">
                            <label for="">Numero de seguro*</label>
                            <input type="text" class="form-control" name="insurance_no" value="{{$vehiculo->insurance_no}}"></div>


                             <div class="form-group">
                            <label for="">Fecha de vencimiento del seguro*</label>
                            <input type="text" class="form-control datepicker" data-format="D, dd MM yyyy"  value="{{$vehiculo->insurance_expiry_date}}" name="insurance_expiry_date">
                            
                        </div>
                            
                            
                            <div class="form-group">
                                <label for="">Ciudad*</label>
                                <select class="form-control" name="city_id">
                                    @foreach($ciudad as $sta)
                                    <option value="{{$sta->city_id}}">{{$sta->name}}</option>
                                    @endforeach

                                </select>
                            </div>

                              <div class="form-group">
                            <label for="">Impuesto de Circulación*</label>
                            <input type="text" class="form-control" name="road_taxt" value="{{$vehiculo->road_taxt}}"></div>

                              <div class="form-group">
                            <label for="">Placas*</label>
                            <input type="text" class="form-control" name="plate" value="{{$vehiculo->plate}}"></div>
                             

                             <div class="form-group">
                                <label for="">Tipo*</label>
                                <select class="form-control" name="vehicle_type_id">
                                    @foreach($tipo as $sta)
                                    <option value="{{$sta->vehicle_type_id}}">{{$sta->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                              <div class="form-group">
                            <label for="">Año*</label>
                            <input type="text" class="form-control" name="year" value="{{$vehiculo->year}}"></div>

                            <div class="form-group">
                            {{ Form::label ('imagen', 'Imagen *', ['class' => 'form-label']) }}
                            <div class="controls">
                                <i class=""></i>
                                @if($vehiculo->vehicle_type_id)
                                  {{ Form::file ('img_src', ['class' => 'custom-file-input', 'vehicle_type_id' => 'img_src']) }}
                                @else
                                  {{ Form::file ('img_src', ['class' => 'custom-file-input', 'vehicle_type_id' => 'img_src', 'required' =>'required']) }}
                                @endif
                                @if($errors->first('img_src'))
                                  <div class="alert alert-error alert-dismissible fade in">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                      {{ $errors->first('img_src') }}
                                  </div>
                                @endif
                            </div>
                            <p>Medidas sugeridas: 1220 * 790</p>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" style="top:-60px;">
                                {{ Html::image($vehiculo->img_src, '', array('class' => 'img-thumbnail img-responsive', 'alt' => '140x140', 'id' => 'imagenChange', 'data-holder-rendered' => 'true')) }}
                                <a class="preview previewGasto" href="/{{ $vehiculo->img_src }}" rel="prettyPhoto"><i class="fa fa-eye"></i></a>
                            </div>
                        </div>


                                                    @if($vehiculo->vehicle_id)
                                                    {{ Form::hidden ('_method', 'PUT', ['vehicle_id' => 'methodo']) }}
                                                    @endif

                                                    <div class="text-right" style="margin-top:120px;">
                                                        {{ link_to('/plataforma/Vehicle/', 'Cancelar', ['class' => 'btn btn-warning']) }}
                                                        {{ Form::submit('Guardar', ['class' => 'btn btn-success', 'type' => 'submit', 'vehicle_id' => 'guardar']) }}
                                                    </div>
                                                    {{ csrf_field() }}
                                                    {{ Form::close() }}


                                                </div>
                                            </div>
                                        </div>


                                    </section>
                                    <br>
                                    <br>

                                </div>
                                @endsection