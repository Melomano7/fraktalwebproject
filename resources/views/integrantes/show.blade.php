@extends('layouts.app')

@section('title','Integrantes')

@section('content')

 <img class="card-img-top
	   rounded-circle mx-auto d-block" src="/images/{{$integrantes->img_url}}" alt="" 
       style="height: 100px;width: 100px;background-color:#EFEFEF;margin: 30px"
	   >
	   <div class="text-center">
	   <h5 class="card-title">{{$integrantes->nombre}}</h5>
	    <a href="/Integrantes/{{$integrantes->nombre}}/edit" class="btn btn-primary">Editar</a>
	    <a href="/Integrantes/{{$integrantes->nombre}}" class="btn btn-danger">Eliminar</a>
	   </div>

@endsection