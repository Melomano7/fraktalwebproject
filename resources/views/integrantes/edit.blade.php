@extends('layouts.app')

	@section('title','Integrantes Edit')

	@section('content')
	<form class="form-group" method="post" action="/Integrante/{{$integrante->nombre}}"
     enctype="multipart/form-data"
	>
    {{ csrf_field() }}
    {{method_field('PATCH')}}
	
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Nombre</label>
			<input type="text" class="form-control" name="nombre" value="{{$integrante->nombre}}">
			
		</div>
		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Correo</label>
			<input type="text" class="form-control" name="email" value="{{$integrante->email}}">
			
		</div>

		<div class="form-group">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<label for="">Avatar</label>
			<input type="file" name="img_url">
			<br>
		</div>
		<br>
		<div class="text-center">
		<input class="btn btn-success bg-info" type="submit" value="Editar">
		</div>
			
		</form>
	@endsection
