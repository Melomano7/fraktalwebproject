<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class vehicle_driver extends Model
{
    protected $table = 'vehicle_drivers';
   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_id','vehicle_id'
            ];
}
