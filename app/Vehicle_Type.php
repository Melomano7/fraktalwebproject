<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Vehicle_Type extends Model
{
     protected $table = 'vehicle__types';
   protected $primaryKey = 'vehicle_type_id';
   protected $fillable = [
        'name', 'max_persons', 'base_fare', 'min_fare', 'price_per_min','cancellation_price','img_src','min_price_per_distance','max_price_per_distance','base_fare_per_minute','city_id',
    ];
}
