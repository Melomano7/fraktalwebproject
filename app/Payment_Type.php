<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Payment_Type extends Model
{
    protected $table = 'payment__types';
    protected $primaryKey = 'payment_type_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
