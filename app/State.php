<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
   protected $table = 'states';
    protected $primaryKey = 'state_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'short_name'
    ];

    public function cities(){
        return $this->hasMany('fraktalwebproject\City','state_id','state_id');
    }

}
