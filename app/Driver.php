<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
 protected $table = 'drivers';
    protected $primaryKey = 'driver_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'vehicle_id','driver_licence','driver_licence_img_src','license_expiry_date','banck_account','about','home_address','status','driver_type', 'user_id'
    ];

     public function Vehicle(){
     	return $this->hasMany('fraktalwebproject\Vehicle', 'vehicle_id', 'vehicle_id');
    }

    public function user(){
      return $this->hasOne('App\User','user_id','user_id');
    }
}
