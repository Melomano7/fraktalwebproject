<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Roles extends Model{
  protected $table = 'roles';
  protected $primaryKey = '';
  protected $fillable =  ['rol'];

  public function user(){
    return $this->belongsTo('fraktalwebproject\User','id_rol','id_rol');
  }

}
