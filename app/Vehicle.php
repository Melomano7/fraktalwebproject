<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
 protected $table = 'vehicles';
    protected $primaryKey = 'vehicle_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','title','status','vehicle_model_id','manufacturer_id','color','insurance_no','insurance_expiry_date','city_id','road_taxt','plate','vehicle_type_id','year','img_src','driver_id'
            ];

            public function Drivers(){
            return $this->hasMany('fraktalwebproject\Drivers', 'driver_id', 'driver_id');
      
    }
}
