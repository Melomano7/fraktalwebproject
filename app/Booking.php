<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    / protected $table = 'users';
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'id_rol', 'imagen',
    ];
}
