<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Payment_Type;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class Payment_TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Payment_Type::all();
      return view('plataforma.payment_type.index',compact('us'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new  Payment_Type();
      $pos = [];
      for ($i=1; $i <= count(Payment_Type::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
       
      // return $data;
      return view('plataforma.payment_type.save')->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|alpha|min:4',
            
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $state = Payment_Type::create($inputs);
        if($state){
          session()->flash('success','¡Tipo de Pago Creado!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear el tipo de pago, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Payment_Type');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Payment_Type::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Payment_Type::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
     
      return view('plataforma.payment_type.save')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $inputs = Request::all();
      $rules = [
             'name' => 'required|alpha|min:4',
            
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $states = Payment_Type::findOrFail($id);
        $states->fill($inputs)->save();
         return redirect()->to('plataforma/Payment_Type');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Payment_Type::destroy($id);
    return redirect()->to('plataforma/Payment_Type');
    }
}
