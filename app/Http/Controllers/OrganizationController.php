<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Organization;
use fraktalwebproject\City;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Organization::all();
         $nState = City::all();
      return view('plataforma.organization.index',compact('us','nState'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Organization();
      $pos = [];
      for ($i=1; $i <= count(Organization::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
        $state=City::all();
      // return $data;
      return view('plataforma.organization.save',compact('state'))->with($data);
      
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|min:4|max:100',
            'rfc' => 'required|min:12|max:30',
            'address' => 'required|min:4|max:30',
            'cp' => 'required|numeric|min:5',
            'position_company_representative' => 'required|min:4|max:30',
            'company_representative' => 'required|min:4|max:30',
          
        ];
     $messages = [
      'company_representative.required' => 'El campo Representante de compañia es obligatorio',
          'company_representative.min' => 'El campo Representante de compañia debe tener al menos 4 caracteres',
          'company_representative.max' => 'El Campo Representante de compañia debe maximo 30 caracteres',
          'position_company_representative.required' => 'El Cargo es obligatorio',
          'position_company_representative.min' => 'El Cargo debe tener al menos 4 caracteres',
          'position_company_representative.max' => 'El Cargo debe tener maximo 30 caracteres',
          'cp.numeric' => 'El Código Postal es obligatorio',
          'cp.min' => 'El Código Postal debe tener por lo menos 5 caracteres',
          'name.required' => 'El campo Nombre es obligatorio',
          'name.min' => 'El Nombre debe tener al menos 4 caracteres',
          'name.max' => 'El Nombre debe maximo 100 caracteres',
          'rfc.required' => 'El campo RFC es obligatorio',
          'rfc.min' => 'El campo RFC debe tener al menos 12 caracteres',
          'rfc.max' => 'El campo RFC debe tener maximo 30 caracteres',
          'address.required' => 'El campo Dirección es obligatorio',
          'address.min' => 'El campo  Dirección debe tener al menos 4 caracteres',
          'address.max' => 'El campo  Dirección debe tener maximo 30 caracteres',
          
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $cities = Organization::create($inputs);
        if($cities){
          session()->flash('success','¡Empresa Creada!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear una empresa, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Organization');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Organization::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Organization::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      $state=City::all();
      return view('plataforma.Organization.save',compact('state'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $inputs = Request::all();
      $rules = [
          'name' => 'required|min:4|max:100',
            'rfc' => 'required|min:12|max:30',
            'address' => 'required|min:4|max:30',
              'cp' => 'required|numeric|min:5',
              'company_representative' => 'required|min:4|max:30',
            'position_company_representative' => 'required|min:4|max:30',
          
        ];
     $messages = [
      'company_representative.required' => 'El campo Representante de compañia es obligatorio',
          'company_representative.min' => 'El campo Representante de compañia debe tener al menos 4 caracteres',
          'company_representative.max' => 'El Campo Representante de compañia debe maximo 30 caracteres',
          'position_company_representative.required' => 'El Cargo es obligatorio',
          'position_company_representative.min' => 'El Cargo debe tener al menos 4 caracteres',
          'position_company_representative.max' => 'El Cargo debe tener maximo 30 caracteres',
       'cp.numeric' => 'El Código Postal es obligatorio',
          'cp.min' => 'El Código Postal debe tener por lo menos 5 caracteres',
         'name.required' => 'El campo Nombre es obligatorio',
          'name.min' => 'El Nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El Nombre solo debe de contener letras',
          'name.max' => 'El Nombre debe maximo 100 caracteres',
          'rfc.required' => 'El campo RFC es obligatorio',
          'rfc.min' => 'El campo RFC debe tener al menos 12 caracteres',
          'rfc.max' => 'El campo RFC debe tener maximo 30 caracteres',
          'address.required' => 'El campo Dirección es obligatorio',
          'address.min' => 'El campo  Dirección debe tener al menos 4 caracteres',
          'address.max' => 'El campo  Dirección debe tener maximo 30 caracteres',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $ciudades = Organization::findOrFail($id);
        $ciudades->fill($inputs)->save();
         return redirect()->to('plataforma/Organization');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Organization::destroy($id);
    return redirect()->to('plataforma/Organization');
    }
}
