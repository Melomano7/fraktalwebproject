<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Vehicle_Model;
use fraktalwebproject\Manufacturer;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class Vehicle_ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Vehicle_Model::all();
          $fabricante=Manufacturer::all();
      return view('plataforma.vehicle_model.index',compact('us','fabricante'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Vehicle_Model();
      $pos = [];
      for ($i=1; $i <= count(Vehicle_Model::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
       
      // return $data;
       $fabricante=Manufacturer::all();
      return view('plataforma.vehicle_model.save',compact('fabricante'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|min:4',
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          
        
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $state = Vehicle_Model::create($inputs);
        if($state){
          session()->flash('success','Modelo Creado!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear el Modelo, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Vehicle_Model');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Vehicle_Model::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Vehicle_Model::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
     $fabricante=Manufacturer::all();
      return view('plataforma.vehicle_model.save',compact('fabricante'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $inputs = Request::all();
      $rules = [
             'name' => 'required|min:4',
            
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $states = Vehicle_Model::findOrFail($id);
        $states->fill($inputs)->save();
         return redirect()->to('plataforma/Vehicle_Model');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Vehicle_Model::destroy($id);
    return redirect()->to('plataforma/Vehicle_Model');
    }
}
