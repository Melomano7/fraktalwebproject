<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Vehicle;
use fraktalwebproject\Manufacturer;
use fraktalwebproject\City;
use fraktalwebproject\Vehicle_Model;
use fraktalwebproject\Vehicle_Type;
use fraktalwebproject\Driver;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;
use Carbon\Carbon;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Vehicle::all();
         $nState = Manufacturer::all();
         $city = City::all();
         $type = Vehicle_Type::all(); 
         $modelo = Vehicle_Model::all();
         $con = Driver::all();   
      return view('plataforma.vehicle.index',compact('us','nState','city','type','modelo','con'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Vehicle();
      $pos = [];
      for ($i=1; $i <= count(Vehicle::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'vehiculo' => $datos,
        'posiciones' => $pos
      ];
        $fabricante=Manufacturer::all();
        $ciudad = City::all();
        $tipo = Vehicle_Type::all();  
        $modelo = Vehicle_Model::all();
        $con = Driver::all(); 
      // return $data;
      return view('plataforma.vehicle.save',compact('fabricante','ciudad','tipo','modelo','con'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
$date = str_replace("-", "", $inputs['insurance_expiry_date']);
$inputs['insurance_expiry_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
            'title' => 'required|min:4|max:30',
            'status' => 'required|alpha|min:4|max:30',
            
            'color' => 'required|alpha|min:4|max:30',
            'insurance_no' => 'required|min:8|max:30',
            'insurance_expiry_date' => 'required|date_format:Y-m-d',
           'road_taxt' => 'required|numeric',
           'plate' => 'required|min:6|max:10',
            'year' => 'required|numeric',
             'img_src' => 'required',
          
        ];
     $messages = [
          'img_src.required' => 'Se requiere de una imagen',
          'title.required' => 'El campo nombre es obligatorio',
          'title.min' => 'El nombre debe tener al menos 4 caracteres',
          'title.max' => 'El nombre debe maximo 30 caracteres',
          'status.required' => 'El campo status es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
          'color.required' => 'El campo color es obligatorio',
          'color.min' => 'El campo color debe tener al menos 4 caracteres',
          'color.alpha' => 'El campo color solo debe de contener letras',
          'color.max' => 'El campo color debe tener maximo 30 caracteres',
          'insurance_no.required' => 'El campo numero de seguro es obligatorio',
          'insurance_no.min' => 'El campo numero de seguro debe tener al menos 8 caracteres',
          'insurance_no.max' => 'El campo numero de seguro debe tener maximo 30 caracteres',
          'insurance_expiry_date.required' => 'El campo Fecha de vencimiento del seguro es obligatorio',
          'insurance_expiry_date.date_format' => 'En el campo Fecha de vencimiento del seguro la fecha debe de terner el formato yyyy-mm-dd',
          'road_taxt.required' => 'El campo de Impuesto de Circulación es obligatorio',
          'road_taxt.numeric' => 'El campo de Impuesto de Circulación solo debe de contener numeros',
           'plate.required' => 'El campo  de placas es obligatorio',
          'plate.min' => 'El campo plate debe tener al menos 6 caracteres',
          
          'plate.max' => 'El campo de placas debe tener maximo 10 caracteres',
          'year.required' => 'El campo año es obligatorio',
          'year.numeric' => 'El campo año solo debe de contener numeros',
      ];
     $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $file = Request::file('img_src');
         if($file){
          $usuario = Vehicle::create($inputs);
          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $usuario->vehicle_type_id;
          $archive = "img/vehicle/vehicle".$id.".".$extension;

          if($file->move("img/vehicle", $archive)){
            $usuario->img_src = $archive;
            $usuario->save();
            session()->flash('success','Vehiculo creado!');
            return Redirect::to('plataforma/Vehicle');
          }else{
            session()->flash('notice','Ocurrió un error al subir la imagen!');
            return Redirect::to('plataforma/Vehicle');
          }


        }else{
          session()->flash('notice','No se encontró la imagen!');
          return Redirect::to('plataforma/Vehicle');
        }

      }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Vehicle::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Vehicle::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'vehiculo' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      $fabricante=Manufacturer::all();
      $ciudad = City::all();
      $tipo = Vehicle_Type::all();
      $modelo = Vehicle_Model::all(); 
       $con = Driver::all();
      return view('plataforma.vehicle.save',compact('fabricante','ciudad','tipo','modelo','con'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $inputs = Request::all();
          $date = str_replace("-", "", $inputs['insurance_expiry_date']);
$inputs['insurance_expiry_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
           'title' => 'required|min:4|max:30',
            'status' => 'required|alpha|min:4|max:30',
            
            'color' => 'required|alpha|min:4|max:30',
            'insurance_no' => 'required|min:8|max:30',
            'insurance_expiry_date' => 'required|date_format:Y-m-d',
           'road_taxt' => 'required|numeric',
           'plate' => 'required|min:6|max:10',
            'year' => 'required|numeric',
          
        ];
     $messages = [
        'img_src.required' => 'Se requiere de una imagen',
          'title.required' => 'El campo nombre es obligatorio',
          'title.min' => 'El nombre debe tener al menos 4 caracteres',
          'title.max' => 'El nombre debe maximo 30 caracteres',
          'status.required' => 'El campo status es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
          
          'color.required' => 'El campo color es obligatorio',
          'color.min' => 'El campo color debe tener al menos 4 caracteres',
          'color.alpha' => 'El campo color solo debe de contener letras',
          'color.max' => 'El campo color debe tener maximo 30 caracteres',
          'insurance_no.required' => 'El campo numero de seguro es obligatorio',
          'insurance_no.min' => 'El campo numero de seguro debe tener al menos 8 caracteres',
          'insurance_no.max' => 'El campo numero de seguro debe tener maximo 30 caracteres',
          'insurance_expiry_date.required' => 'El campo Fecha de vencimiento del seguro es obligatorio',
          'insurance_expiry_date.date_format' => 'En el campo Fecha de vencimiento del seguro la fecha debe de terner el formato yyyy-mm-dd',
          'road_taxt.required' => 'El campo de Impuesto de Circulación es obligatorio',
          'road_taxt.numeric' => 'El campo de Impuesto de Circulación solo debe de contener numeros',
           'plate.required' => 'El campo  de placas es obligatorio',
          'plate.min' => 'El campo plate debe tener al menos 6 caracteres',
          
          'plate.max' => 'El campo de placas debe tener maximo 10 caracteres',
          'year.required' => 'El campo año es obligatorio',
          'year.numeric' => 'El campo año solo debe de contener numeros',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      Request::flash();
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $file = Request::file('img_src');
        $banner = Vehicle::findOrFail($id);
        if($file){

          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $banner->vehicle_type_id;
          $archive = "img/vehicle/vehicle".$id.".".$extension;

          if($file->move("img/vehicle", $archive)){

        $banner->fill($inputs)->save();

            session()->flash('success','Vehiculo actualizado!');
            return Redirect::to('plataforma/Vehicle');

          }else{

             $banner->fill($inputs)->save();
            session()->flash('notice','Ocurrio un problema al subir la imagen!');
            return Redirect::to('plataforma/Vehicle');
          }

        }else{
            $banner->fill($inputs)->save();
          session()->flash('success','Vehiculo actualizado!');
          return Redirect::to('plataforma/Vehicle');

        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Vehicle::destroy($id);
    return redirect()->to('plataforma/Vehicle');
    }
}
