<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\User;
use fraktalwebproject\Roles;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=User::all();
         $nRol = Roles::all();
      return view('plataforma.usuarios.index',compact('us','nRol'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new User();
      $pos = [];
      for ($i=1; $i <= count(User::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'user' => $datos,
        'posiciones' => $pos
      ];
        $rol=Roles::all();
      // return $data;
      return view('plataforma.usuarios.save',compact('rol'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|min:4',
            'password' => 'required|min:4',
          'email' => 'required|email|unique:users,email',
          'rol_id' => 'required|numeric',
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'password.required' => 'El campo contraseña es obligatorio',
          'password.min' => 'El contraseña debe tener al menos 4 caracteres',
          'email.unique' => 'Este correo ya esta en uso',
          'email.email' => 'El correo debe de tener este formato example@email.com',
          'email.required' => 'El campo correo es obligatorio',
          'rol_id.required' => 'El campo Rol es obligatorio',
          'rol_id.numeric' => 'El campo Rol solo debe de contener numeros',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
       $inputs['password']=Hash::make($inputs['password']);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $usuario = User::create($inputs);
        if($usuario){
          session()->flash('success','¡Usuario Creado!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear el usuario, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Usuarios');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = User::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(User::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'user' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      $rol=Roles::all();
      return view('plataforma.usuarios.save',compact('rol'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $inputs = Request::all();
      $rules = [
            'name' => 'required|alpha|min:4',
            'password' => 'required|min:4',
          'email' => 'required|email',
          'rol_id' => 'required|numeric',
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'password.required' => 'El campo contraseña es obligatorio',
          'password.min' => 'El contraseña debe tener al menos 4 caracteres',
          'email.unique' => 'Este correo ya esta en uso',
          'email.email' => 'El correo debe de tener este formato example@email.com',
          'email.required' => 'El campo correo es obligatorio',
          'rol_id.required' => 'El campo Rol es obligatorio',
          'rol_id.numeric' => 'El campo Rol solo debe de contener numeros',
      ];
      $inputs['password']=Hash::make($inputs['password']);
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $usuarios = User::findOrFail($id);
         if($inputs['password']==''){
          $usuarios->fill($inputs)->save()->except('password');
        }
        $usuarios->fill($inputs)->save();
         return redirect()->to('plataforma/Usuarios');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    User::destroy($id);
    return redirect()->to('plataforma/Usuarios');
    }
}
