<?php

namespace fraktalwebproject\Http\Controllers;

use Request;
use Auth;
use Log;
use Hash;
use Carbon\Carbon;

use fraktalwebproject\User;
use fraktalwebproject\City;
use fraktalwebproject\Manufacturer;
use fraktalwebproject\Vehicle_Model;


class ApiCtrl extends Controller{
    

    // --------------------------- Public vars ---------------------

  public $dominio = "http://www.our-domian.com/api/";

  public $android_version = 1.0;
  public $android_force_update = true;

  public $ios_version = 1.0;
  public $ios_force_update = true;

  public $test_location = [
    'lat' => 19.702873,
    'lng' => -101.192330
  ];

  public $test_location_1 = [
    'lat' => 19.702870,
    'lng' => -101.192400
  ];




  // --------------------------- App version ---------------------
  public function checkVersion(){
    $inputs = Request::all();
    if( isset($inputs['platform']) ) {
      return response()->json([
        'version' => $inputs['platform'] == "android" ? $this->android_version : $this->ios_version,
        'force_update' => $inputs['platform'] == "android" ? $this->android_force_update : $this->ios_force_update
      ], 200);
    }else{
      return response()->json(['status' => 401, 'msg' => 'invalid_platform'], 401);
    }
  }


  // --------------------------- Haversine ---------------------------

  public function getDistance($p1, $p2){
    $R = 6378137; // Earth’s mean radius in meter
    $dLat = $this->rad($p2['lat'] - $p1['lat']);
    $dLong = $this->rad($p2['lng'] - $p1['lng']);
    $a = sin($dLat / 2) * sin($dLat / 2) + cos($this->rad($p1['lat'])) * cos($this->rad($p2['lat'])) * sin($dLong / 2) * sin($dLong / 2);
    $c = 2 * atan2(sqrt($a), sqrt(1 - $a));
    $d = $R * $c;
    return $d; // returns the distance in meter
  }

  public function rad($x){
    return $x * pi() / 180;
  }

  // --------------------------- Auth methods ----------------------

  public function login(){
    $inputs = Request::all();
    $user = User::where('email', $inputs['email'])->first();
    if($user){
      if(Auth::attempt(['email' => $inputs['email'], 'password' => $inputs['password'],  'id_rol' => '3'], true)){
        return response()->json(['success' => true, "user" => $user ,"msg" => null]);
      }else{
        return response()->json(['success' => false, "user" => $user ,"msg" => "Credenciales inválidas"]);
      }
    }else{
      return response()->json(['success' => false, 'msg' => 'No existe ningún usuario registrado con ese correo']);
    }
  }

  public function authFacebook(){
    $inputs = Request::all();
    $user = User::where('email', $inputs['email'])->first();
    if($user){
      $user->fill($inputs)->save();
      Auth::login($user, true);
      return response()->json(['success' => true, "user" => $user ,"msg" => null]);
    }else{
      if($user = User::create($inputs)){
        Auth::login($user, true);
        return response()->json(['success' => true, "user" => $user ,"msg" => null]);
      }else{
        return response()->json(['success' => false, "msg" => "Lo sentimos, no pudimos guardar tus datos, intentalo de nuevo!"]);
      }
    }
  }

  public function register(){
    $inputs = Request::all();
    $user = User::where('email', $inputs['email'])->first();
    if($user){
      return response()->json(['success' => false, 'msg' => 'Ya existe un usuario con ese correo']);
    }else{
      $data = [
        'name' => $inputs['name'],
        'email' => $inputs['email'],
        'password' => Hash::make($inputs['password']),
        'imagen' => Request::root()."/img/default_profile.png",
        'id_rol' => 3,
        'account_type' => 'platform'
      ];
      if($user = User::create($data)){
        Auth::login($user, true);
        return response()->json(['success' => true, "user" => $user ,"msg" => null]);
      }else{
        return response()->json(['success' => false, "msg" => "Lo sentimos, no te registramos, intentalo de nuevo!"]);
      }
    }
  }
  
  // --------------------------- Api app methods ----------------------


  public function getCities($state_id){
    return City::where('state_id', $state_id)->where('status', 'active')->get()->pluck('name', 'city_id');
  }
  public function getModel($manufacturer_id){
    return Vehicle_Model::where('manufacturer_id', $manufacturer_id)->get()->pluck('name', 'vehicle_model_id');
  }

}
