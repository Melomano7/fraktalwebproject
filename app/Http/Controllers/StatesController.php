<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\State;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class StatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=State::all();
      return view('plataforma.states.index',compact('us'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new State();
      $pos = [];
      for ($i=1; $i <= count(State::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
       
      // return $data;
      return view('plataforma.states.save')->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|alpha|min:4',
            'short_name' => 'required|alpha|max:4',
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'short_name.required' => 'El campo short_name es obligatorio',
          'short_name.max' => 'El campo short_name debe tener maximo 4 caracteres',
          'short_name.alpha' => 'El campo short_name solo debe de contener letras',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $state = State::create($inputs);
        if($state){
          session()->flash('success','¡State Creado!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear el usuario, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/States');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = State::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(State::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
     
      return view('plataforma.states.save')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $inputs = Request::all();
      $rules = [
             'name' => 'required|alpha|min:4',
            'short_name' => 'required|alpha|max:4',
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'short_name.required' => 'El campo short_name es obligatorio',
          'short_name.max' => 'El campo short_name debe tener maximo 4 caracteres',
          'short_name.alpha' => 'El campo short_name solo debe de contener letras',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $states = State::findOrFail($id);
        $states->fill($inputs)->save();
         return redirect()->to('plataforma/States');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    State::destroy($id);
    return redirect()->to('plataforma/States');
    }
}
