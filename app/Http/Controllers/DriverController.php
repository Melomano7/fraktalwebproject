<?php

namespace fraktalwebproject\Http\Controllers;

use fraktalwebproject\Driver;
use fraktalwebproject\Driver_vehicle;
use fraktalwebproject\Vehicle;
use fraktalwebproject\Vehicle_Type;
use fraktalwebproject\City;
use fraktalwebproject\State;
use fraktalwebproject\User;
use fraktalwebproject\Manufacturer;
use fraktalwebproject\Vehicle_Model;


use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;
use Carbon\Carbon;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Driver::all();
         $vehicle = Vehicle::all();
         $type = Vehicle_Type::all();
         $usuarios=User::all(); 
      return view('plataforma.driver.index',compact('us','vehicle','type','usuarios'));
    }



    public function registerDriverForm(){

      $data = [
        'states' => State::all()->pluck('name', 'state_id'),
        'vehicles' => Vehicle::all()->pluck('plate', 'vehicle_id'),
        'manufacturer' => Manufacturer::all()->pluck('name', 'manufacturer_id'),
        'modelo' => Vehicle_Model::all()->pluck('name', 'vehicle_model_id'),
        'con'=> Driver::all()->pluck('name', 'driver_id'),
        'ciudad'=> City::all()->pluck('name', 'city_id'),
        'type' => Vehicle_Type::all()->pluck('name', 'vehicle_type_id'),
        'estado'=>State::all()->pluck('name', 'state_id'),
        // manufacturer
      ];
      // return $data;
      return view('driverRegister.save')->with($data);
    }


    public function registerDriverRequest(){
      $inputs = Request::all();
      //return $inputs;
     
      //$inputUser=Request::except('title', 'driver_id','status','manufacturer_id','vehicle_model_id','year','color','insurance_no','insurance_expiry_date','road_taxt','plate','vehicle_type_id','img_src','driver_licence','license_expiry_date','banck_account','about','home_address','state_id','vehicle_id','driver_type');
     // $inputs2= Request::except('name', 'email','phone','title', 'driver_id','status','manufacturer_id','vehicle_model_id','year','color','insurance_no','insurance_expiry_date','road_taxt','plate','vehicle_type_id','img_src');
      $inputs3=Request::except('name', 'email','phone','driver_licence','license_expiry_date','banck_account','about','home_address','state_id','vehicle_id','driver_type');

      $date = str_replace("-", "", $inputs['license_expiry_date']);
      $inputs['license_expiry_date'] = Carbon::parse($date)->format('Y-m-d');
      $inputs['insurance_expiry_date'] = Carbon::parse($date)->format('Y-m-d');
      $inputs3['insurance_expiry_date'] = Carbon::parse($date)->format('Y-m-d');


     if($inputs['driver_type'] == "Propietario"){

      $rules = [
        'name' => 'required|min:4',
        'phone' => 'required|min:10',
        'license_expiry_date' => 'required|date_format:Y-m-d',
        'email' => 'required|email|unique:users,email',
        'driver_licence' => 'required|max:30|min:10',
        'banck_account' => 'required|min:16|max:20',
        'about' => 'required',
        'home_address' => 'required',
          'title' => 'required|min:4|max:30',
          'status' => 'required|alpha|min:4|max:30',
          'color' => 'required|alpha|min:4|max:30',
          'insurance_no' => 'required|min:8|max:30',
          'insurance_expiry_date' => 'required|date_format:Y-m-d',
          'road_taxt' => 'required|numeric',
          'plate' => 'required|min:6|max:10',
          'year' => 'required|numeric',

      ];
        
      $messages = [
        'email.unique' => 'Este correo ya esta en uso',
        'email.email' => 'El correo debe de tener este formato example@email.com',
        'email.required' => 'El campo correo es obligatorio',
        'name.required' => 'El campo nombre es obligatorio',
        'phone.required' => 'El Teléfono es obligatorio',
        'phone.min' => 'El Teléfono debe tener 10 digitos',
        'name.min' => 'El campo nombre debe tener al menos 4 caracteres',
        'driver_licence_img_src.required' => 'Se requiere de una imagen',
        'status.required' => 'El campo status es obligatorio',
        'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
        'status.alpha' => 'El campo Estatus solo debe de contener letras',
        'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
        'driver_licence.required' => 'El campo licencia de manejo es obligatorio',
        'driver_licence.min' => 'El campo licencia de manejo debe tener al menos 10 caracteres',
        'driver_licence.max' => 'El campo licencia de manejo debe tener maximo 30 caracteres',
        'banck_account.required' => 'El campo cuenta de banco es obligatorio',
        'banck_account.min' => 'El campo cuenta de banco debe tener al menos 16 caracteres',
        'banck_account.max' => 'El campo cuenta de banco debe tener maximo 30 caracteres',
        'license_expiry_date.required' => 'El campo Fecha de vencimiento de la licencia es obligatorio',
        'license_expiry_date.date_format' => 'En el campo Fecha de vencimiento de la licencia debe de terner el formato yyyy-mm-dd',
        'about.required' => 'El campo Acerca de es obligatorio',
        'operating_area.required' => 'El campo area de operacion es obligatorio',
        'home_address.required' => 'El campo  direccion del conductor es obligatorio',
        'img_src.required' => 'Se requiere de una imagen',
        'title.required' => 'El campo nombre es obligatorio',
        'title.min' => 'El nombre debe tener al menos 4 caracteres',
        'title.max' => 'El nombre debe maximo 30 caracteres',
        'status.required' => 'El campo status es obligatorio',
        'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
        'status.alpha' => 'El campo Estatus solo debe de contener letras',
        'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
        'color.required' => 'El campo color es obligatorio',
        'color.min' => 'El campo color debe tener al menos 4 caracteres',
        'color.alpha' => 'El campo color solo debe de contener letras',
        'color.max' => 'El campo color debe tener maximo 30 caracteres',
        'insurance_no.required' => 'El campo numero de seguro es obligatorio',
        'insurance_no.min' => 'El campo numero de seguro debe tener al menos 8 caracteres',
        'insurance_no.max' => 'El campo numero de seguro debe tener maximo 30 caracteres',
        'insurance_expiry_date.required' => 'El campo Fecha de vencimiento del seguro es obligatorio',
        'insurance_expiry_date.date_format' => 'En el campo Fecha de vencimiento del seguro la fecha debe de terner el formato yyyy-mm-dd',
        'road_taxt.required' => 'El campo de Impuesto de Circulación es obligatorio',
        'road_taxt.numeric' => 'El campo de Impuesto de Circulación solo debe de contener numeros',
        'plate.required' => 'El campo  de placas es obligatorio',
        'plate.min' => 'El campo plate debe tener al menos 6 caracteres',
        'plate.max' => 'El campo de placas debe tener maximo 10 caracteres',
        'year.required' => 'El campo año es obligatorio',
        'year.numeric' => 'El campo año solo debe de contener numeros',
      ];
     }
     else{
      $rules = [
        'name' => 'required|min:4',
        'phone' => 'required|min:10',
        'license_expiry_date' => 'required|date_format:Y-m-d',
        'email' => 'required|email|unique:drivers,email',
        'driver_licence' => 'required|max:30|min:10',
        'banck_account' => 'required|min:16|max:20',
        'about' => 'required',
        'home_address' => 'required',

      ];
      $messages = [
        'email.unique' => 'Este correo ya esta en uso',
        'email.email' => 'El correo debe de tener este formato example@email.com',
        'email.required' => 'El campo correo es obligatorio',
        'name.required' => 'El campo nombre es obligatorio',
        'phone.required' => 'El Teléfono es obligatorio',
        'phone.min' => 'El Teléfono debe tener 10 digitos',
        'name.min' => 'El campo nombre debe tener al menos 4 caracteres',
        'driver_licence_img_src.required' => 'Se requiere de una imagen',
        'status.required' => 'El campo status es obligatorio',
        'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
        'status.alpha' => 'El campo Estatus solo debe de contener letras',
        'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
        'driver_licence.required' => 'El campo licencia de manejo es obligatorio',
        'driver_licence.min' => 'El campo licencia de manejo debe tener al menos 10 caracteres',
        'driver_licence.max' => 'El campo licencia de manejo debe tener maximo 30 caracteres',
        'banck_account.required' => 'El campo cuenta de banco es obligatorio',
        'banck_account.min' => 'El campo cuenta de banco debe tener al menos 16 caracteres',
        'banck_account.max' => 'El campo cuenta de banco debe tener maximo 30 caracteres',
        'license_expiry_date.required' => 'El campo Fecha de vencimiento de la licencia es obligatorio',
        'license_expiry_date.date_format' => 'En el campo Fecha de vencimiento de la licencia debe de terner el formato yyyy-mm-dd',
        'about.required' => 'El campo Acerca de es obligatorio',
        'operating_area.required' => 'El campo area de operacion es obligatorio',
        'home_address.required' => 'El campo  direccion del conductor es obligatorio',
      ];
    }

 
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{    
        
        // Lo primero que se debe crear es el usuario, en el modelo de "User"
            // Con los datos básicos (nombre, correo, password, id_rol, phone, etcc)
            // Crear un generador de contraseñas:
            // La contraseña debe estar compuesta por 
            // - las primeras dos letras del nombre, 
            // - el año actual, 
            // - las 2 primeras letras del correo, 
            // - los 2 ultimos digitos del telefono
            // Ejemplo: nombre: Juan Perez Lopez, Correo: przLopz@dominio.com, 4434998790
            // La contraseña será: ju2019pr90, se dará despues acceso a los drivers para que la cambien desde la app

            // Cuando se inserte el usuario | $user =  User::create($datos_user); \ -> Ya podemos obtener el id para insertarselo al driver
            // $user->user_id;
    
        
        $nombre = substr($inputs['name'], 0, 2);
        $año = date('Y');
        $correo = substr($inputs['email'], 0, 2);
        $telefono = substr($inputs['phone'], -2);

        
        $datos_del_user=[
        'name'=>$inputs['name'],
        'email'=>$inputs['email'],
        'city_id'=>$inputs['city_id'],
        'phone'=>$inputs['phone'],
        'password'=>$nombre.$año.$correo.$telefono,
        'rol_id'=>3,
        ];
        
        
        $us = User::create($datos_del_user);
        $usid = $us->user_id;

          if($inputs['driver_type'] == "Chofer"){
        
         $datos_del_driver=[
        'driver_licence'=>$inputs['driver_licence'],
        'license_expiry_date'=>$inputs['license_expiry_date'],
        'banck_account'=>$inputs['banck_account'],
        'about'=>$inputs['about'],
        'home_address'=>$inputs['home_address'],
        'vehicle_id'=>$inputs['vehicle_id'],
        'driver_type'=>$inputs['driver_type'],
        'user_id'=>$usid,
        ];
            $usuario = Driver::create($datos_del_driver);
            $id = $usuario->driver_id;
          }


        if($inputs['driver_type'] == "Propietario"){

         $datos_del_driver=[
        'driver_licence'=>$inputs['driver_licence'],
        'license_expiry_date'=>$inputs['license_expiry_date'],
        'banck_account'=>$inputs['banck_account'],
        'about'=>$inputs['about'],
        'home_address'=>$inputs['home_address'],
        'vehicle_id'=>null,
        'driver_type'=>$inputs['driver_type'],
        'user_id'=>$usid,
        ];
    
          $usuario = Driver::create($datos_del_driver);
          $id = $usuario->driver_id;
             
            
            
            $vehi = Vehicle::create($inputs3);
            $id2 = $vehi->vehicle_id;
            $editar = Vehicle::find($id2);
            $mod=[
            'driver_id'=>$id
            ];

            $editar->fill($mod)->save();
            

            $movie = new Driver_vehicle;
            $movie->driver_id = $id;
            $movie->vehicle_id = $id2;
            $movie->save();
            $file = Request::file('img_src');
            if($file){
              $archive = "";
              $extension = $file->getClientOriginalExtension();
              $archive = "img/vehicle/vehicle".$id2.".".$extension;
              if($file->move("img/vehicle", $archive)){
                $vehi->img_src = $archive;
                $vehi->save();
                session()->flash('success','Vehiculo creado!');
                return Redirect::to('plataforma/Vehicle');
              }else{
                session()->flash('notice','Ocurrió un error al subir la imagen!');
              }
              if($vehi){
                session()->flash('success','¡Vehiculo Creado!');
              }
              else{
              session()->flash('notice','¡Ocurrio un error al crear el vehiculo, intentalo de nuevo!');
            }
            }

         
        }else{
          $movie = new Driver_vehicle;
          $movie->driver_id = $id;
          $movie->vehicle_id = $inputs['vehicle_id'];
          $movie->save();
        }
      }
      return redirect()->to('plataforma/States');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Driver();
      $pos = [];
      for ($i=1; $i <= count(Driver::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'vehiculo' => $datos,
        'posiciones' => $pos
      ];
        $vehicle=Vehicle::all();
        $usuarios=User::all();
        $tipo = Vehicle_Type::all();  
         
       return view('plataforma.driver.save',compact('tipo','vehicle','usuarios'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
$inputs = Request::all();
$date = str_replace("-", "", $inputs['license_expiry_date']);
$inputs['license_expiry_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
         'name' => 'required|min:4',
        'phone' => 'required|min:10',
        'email' => 'required|email|unique:users,email',
        'license_expiry_date' => 'required|date_format:Y-m-d',
        'driver_licence_img_src' => 'required',
        'status' => 'required|alpha|min:4|max:30',
        'driver_licence' => 'required|max:30|min:10',
        'banck_account' => 'required|min:16|max:20',
        'about' => 'required',
        'home_address' => 'required',
            

        ];
     $messages = [
          'phone.required' => 'El Teléfono es obligatorio',
          'phone.min' => 'El Teléfono debe tener 10 digitos',
          'email.unique' => 'Este correo ya esta en uso',
          'email.email' => 'El correo debe de tener este formato example@email.com',
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El campo nombre debe tener al menos 4 caracteres',
          'driver_licence_img_src.required' => 'Se requiere de una imagen',
          'status.required' => 'El campo status es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
           'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'lng.required' => 'El campo Longitud es obligatorio',
          'lng.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'lng.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          'driver_licence.required' => 'El campo licencia de manejo es obligatorio',
          'driver_licence.min' => 'El campo licencia de manejo debe tener al menos 10 caracteres',
          'driver_licence.max' => 'El campo licencia de manejo debe tener maximo 30 caracteres',
          'banck_account.required' => 'El campo cuenta de banco es obligatorio',
          'banck_account.min' => 'El campo cuenta de banco debe tener al menos 16 caracteres',
          'banck_account.max' => 'El campo cuenta de banco debe tener maximo 30 caracteres',
          'license_expiry_date.required' => 'El campo Fecha de vencimiento de la licencia es obligatorio',
          'license_expiry_date.date_format' => 'En el campo Fecha de vencimiento de lalicencia debe de terner el formato yyyy-mm-dd',
          'about.required' => 'El campo Acerca de es obligatorio',
          'operating_area.required' => 'El campo area de operacion es obligatorio',
          'home_address.required' => 'El campo  direccion del conductor es obligatorio',
          'grades.required' => 'El campo calificacion es obligatorio',
          'grades.numeric' => 'El campo calificacion solo debe de contener numeros',
           'speed.required' => 'El campo velocidad es obligatorio',
          'speed.numeric' => 'El campo velocidad debe de ser numerico',
      ];

      $validar = Validator::make($inputs, $rules, $messages);
     
      if($validar->fails()){
       
        return Redirect::back()->withInput(Request::all())->withErrors($validar);

      }else{
       
        $file = Request::file('driver_licence_img_src');
         if($file){

        $nombre = substr($inputs['name'], 0, 2);
        $año = date('Y');
        $correo = substr($inputs['email'], 0, 2);
        $telefono = substr($inputs['phone'], -2);

        
        $datos_del_user=[
        'name'=>$inputs['name'],
        'email'=>$inputs['email'],
        'phone'=>$inputs['phone'],
        'password'=>$nombre.$año.$correo.$telefono,
        'rol_id'=>3,
        ];

        $us = User::create($datos_del_user);

        $usid = $us->user_id;

        $datos_del_driver=[
        'driver_licence'=>$inputs['driver_licence'],
        'license_expiry_date'=>$inputs['license_expiry_date'],
        'banck_account'=>$inputs['banck_account'],
        'about'=>$inputs['about'],
        'home_address'=>$inputs['home_address'],
        'vehicle_id'=>$inputs['vehicle_id'],
        'driver_type'=>$inputs['driver_type'],
        'user_id'=>$usid,
        ];

          $usuario = Driver::create($datos_del_driver);
          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $usuario->driver_id;
          $archive = "img/driver/driver".$id.".".$extension;

          if($file->move("img/driver", $archive)){
            $usuario->driver_licence_img_src = $archive;
            $usuario->save();
            session()->flash('success','Vehiculo creado!');

           return redirect()->to('plataforma/Driver');
          }else{
            session()->flash('notice','Ocurrió un error al subir la imagen!');

            return redirect()->to('plataforma/Driver');
          }


        }else{
          session()->flash('notice','No se encontró la imagen!');
          return redirect()->to('plataforma/Driver');
        }
       
      }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Driver::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Driver::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'vehiculo' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      $vehicle=Vehicle::all();
       $usuarios=User::all();
        $tipo = Vehicle_Type::all();  
         
      // return $data;
      return view('plataforma.driver.save',compact('vehicle','tipo','usuarios'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         
          $inputs = Request::all();
          $date = str_replace("-", "", $inputs['license_expiry_date']);
$inputs['license_expiry_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
       
        'name' => 'required|min:4',
        'phone' => 'required|min:10',
        'email' => 'required|email',
        'license_expiry_date' => 'required|date_format:Y-m-d',
        'status' => 'required|alpha|min:4|max:30',
        'driver_licence' => 'required|max:30|min:10',
        'banck_account' => 'required|min:16|max:20',
        'about' => 'required',
        'home_address' => 'required',
           
          
        ];
     $messages = [
        'phone.required' => 'El Teléfono es obligatorio',
          'phone.min' => 'El Teléfono debe tener 10 digitos',
          'email.unique' => 'Este correo ya esta en uso',
          'email.email' => 'El correo debe de tener este formato example@email.com',
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El campo nombre debe tener al menos 4 caracteres',
          'driver_licence_img_src.required' => 'Se requiere de una imagen',
          'status.required' => 'El campo status es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
           'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'lng.required' => 'El campo Longitud es obligatorio',
          'lng.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'lng.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          'driver_licence.required' => 'El campo licencia de manejo es obligatorio',
          'driver_licence.min' => 'El campo licencia de manejo debe tener al menos 10 caracteres',
          'driver_licence.max' => 'El campo licencia de manejo debe tener maximo 30 caracteres',
          'banck_account.required' => 'El campo cuenta de banco es obligatorio',
          'banck_account.min' => 'El campo cuenta de banco debe tener al menos 16 caracteres',
          'banck_account.max' => 'El campo cuenta de banco debe tener maximo 30 caracteres',
          'license_expiry_date.required' => 'El campo Fecha de vencimiento de la licencia es obligatorio',
          'license_expiry_date.date_format' => 'En el campo Fecha de vencimiento de lalicencia debe de terner el formato yyyy-mm-dd',
          'about.required' => 'El campo Acerca de es obligatorio',
          'operating_area.required' => 'El campo area de operacion es obligatorio',
          'home_address.required' => 'El campo  direccion del conductor es obligatorio',
          'grades.required' => 'El campo calificacion es obligatorio',
          'grades.numeric' => 'El campo calificacion solo debe de contener numeros',
           'speed.required' => 'El campo velocidad es obligatorio',
          'speed.numeric' => 'El campo velocidad debe de ser numerico',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      Request::flash();
    
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        
        $file = Request::file('driver_licence_img_src');
        $banner = Driver::findOrFail($id); 
        $user = User::findOrFail($banner->user_id);
        if($file){
          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $banner->driver_id;
          $archive = "img/driver/driver".$id.".".$extension;

          if($file->move("img/driver", $archive)){
         $datos_del_user=[
        'name'=>$inputs['name'],
        'email'=>$inputs['email'],
        'phone'=>$inputs['phone'],
        ];

        $datos_del_driver=[
        'driver_licence'=>$inputs['driver_licence'],
        'license_expiry_date'=>$inputs['license_expiry_date'],
        'banck_account'=>$inputs['banck_account'],
        'about'=>$inputs['about'],
        'home_address'=>$inputs['home_address'],
        'vehicle_id'=>$inputs['vehicle_id'],
        'driver_type'=>$inputs['driver_type'],
        
        ];
        
        $banner->fill($datos_del_driver)->save();
        $user->fill($datos_del_user)->save();
        
            session()->flash('success','Conductor actualizado!');
            
              return redirect()->to('plataforma/Driver');
            
          }else{
             
             $banner->fill($datos_del_driver)->save();
             $user->fill($datos_del_user)->save();
            session()->flash('notice','Ocurrio un problema al subir la imagen!');
              return redirect()->to('plataforma/Driver');
          }

        }else{
           
            $banner->fill($inputs)->save();
          session()->flash('success','Conductor actualizado!');
           return redirect()->to('plataforma/Driver');
        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Driver::destroy($id);
    return redirect()->to('plataforma/Driver');
    }
}
