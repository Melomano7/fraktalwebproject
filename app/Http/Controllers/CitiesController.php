<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\City;
use fraktalwebproject\State;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=City::all();
         $nState = State::all();
      return view('plataforma.cities.index',compact('us','nState'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new City();
      $pos = [];
      for ($i=1; $i <= count(City::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
        $state=State::all();
      // return $data;
      return view('plataforma.cities.save',compact('state'))->with($data);
      
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            'name' => 'required|alpha|min:4|max:100',
            'lat' => 'required|min:4|max:30',
            'ing' => 'required|min:4|max:30',
            'status' => 'required|alpha|min:4|max:30',
          'state_id' => 'required|numeric',
           'km_range' => 'required|numeric',
     
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'name.max' => 'El nombre debe maximo 100 caracteres',
          'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'status.required' => 'El campo Estatus es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
          'ing.required' => 'El campo Longitud es obligatorio',
          'ing.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'ing.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          'km_range.required' => 'El campo Rango de Km es obligatorio',
          'km_range.numeric' => 'El campo Rango de Km solo debe de contener numeros',
          'state_id.required' => 'El campo Estado es obligatorio',
          'state_id.numeric' => 'El campo Estado solo debe de contener numeros',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $cities = City::create($inputs);
        if($cities){
          session()->flash('success','¡Ciudad Creada!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear city, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Cities');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = City::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(City::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      $state=State::all();
      return view('plataforma.cities.save',compact('state'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $inputs = Request::all();
      $rules = [
            'name' => 'required|alpha|min:4|max:100',
            'lat' => 'required|min:4|max:30',
            'ing' => 'required|min:4|max:30',
            'status' => 'required|alpha|min:4|max:30',
          'state_id' => 'required|numeric',
           'km_range' => 'required|numeric',
           
          
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'name.max' => 'El nombre debe maximo 100 caracteres',
          'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'status.required' => 'El campo Estatus es obligatorio',
          'status.min' => 'El campo Estatus debe tener al menos 4 caracteres',
          'status.alpha' => 'El campo Estatus solo debe de contener letras',
          'status.max' => 'El campo Estatus debe tener maximo 30 caracteres',
          'ing.required' => 'El campo Longitud es obligatorio',
          'ing.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'ing.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          'km_range.required' => 'El campo Rango de Km es obligatorio',
          'km_range.numeric' => 'El campo Rango de Km solo debe de contener numeros',
          'state_id.required' => 'El campo Estado es obligatorio',
          'state_id.numeric' => 'El campo Estado solo debe de contener numeros',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $ciudades = City::findOrFail($id);
        $ciudades->fill($inputs)->save();
         return redirect()->to('plataforma/Cities');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    City::destroy($id);
    return redirect()->to('plataforma/Cities');
    }
}
