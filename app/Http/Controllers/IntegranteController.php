<?php

namespace fraktalwebproject\Http\Controllers;
use Illuminate\Auth\Middleware;
use Illuminate\Http\Request;
use fraktalwebproject\Integrante;
use fraktalwebproject\Http\Requests\StoreTrainerRequest;
use Illuminate\Support\Fecades\Storage;

class IntegranteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //$request->user()->authorizeRoles('admin');
         $integrantes=Integrante::all();
      return view('integrantes.index',compact('integrantes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('integrantes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $integrante=new Integrante();
    if ($request->hasFile('avatar')) { $file = $request->file('avatar'); 
        $name = time().$file->getClientOriginalName(); $file->move(public_path().'/images/', $name); }
        //estancia de Triner
       $integrante->nombre=$request->input('name');
        $integrante->email=$request->input('email');
        $integrante->img_url =$name;
        //metodo save para que se almacene un nuevo recurso en la bd

        $integrante->save();
        //return 'saved';
        return 'guardado';
        return redirect()->route('integrantes.index'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
        $integrante=Integrante::where('nombre','=',$name)->firstOrFail();
      return view('integrantes.edit',compact('integrante'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
   $integrante=$request->except(['_token','_method']);

if ($request->hasFile('avatar')) {
          $int=Integrante::where('nombre','=',$name)->firstOrFail();
          Storage::delete('public/'.$int->img_url);
          $integrante['img_url']=$request->file('img_url')->store('uploads','public')
          ;}

   Integrante::where('nombre',$name)->update($integrante);
       
       return 'update';   

        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_integrante)
    {
    Integrante::destroy($id_integrante);

    }
}
