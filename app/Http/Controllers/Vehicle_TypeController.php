<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Vehicle_Type;
use fraktalwebproject\City;
use fraktalwebproject\Roles;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class Vehicle_TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Vehicle_Type::all();
        $city=City::all();
      return view('plataforma.vehicle_type.index',compact('us','city'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Vehicle_Type();
      $pos = [];
      for ($i=1; $i <= count(Vehicle_Type::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
        $city=City::all();
      // return $data;
      return view('plataforma.vehicle_type.save',compact('city'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
             'name' => 'required|alpha|min:4|max:100',
             'max_persons' => 'required|numeric',
             'base_fare' => 'required|numeric',
             'min_fare' => 'required|numeric',
             'price_per_min' => 'required|numeric',
             'cancellation_price' => 'required|numeric',
            'img_src' => 'required',
            'min_price_per_distance' => 'required|numeric',
            'max_price_per_distance' => 'required|numeric',
            'base_fare_per_minute' => 'required|numeric',
            
        ];
     $messages = [
      'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'name.max' => 'El nombre debe maximo 100 caracteres',
          'max_persons.required' => 'El campo numero maximo de personas es obligatorio',
          'max_persons.numeric' => 'El campo numero maximo de personas solo debe de contener numeros',
          'base_fare.required' => 'El campo Tarifa base es obligatorio',
          'base_fare.numeric' => 'El campo Tarifa base solo debe de contener numeros',
          'min_fare.required' => 'El  campo Tarifa minima base_fare es obligatorio',
          'min_fare.numeric' => 'El campo Tarifa minima base_fare solo debe de contener numeros',
          'price_per_min.required' => 'El campo Precio por minuto es obligatorio',
          'price_per_min.numeric' => 'El campo Precio por minuto solo debe de contener numeros',
          'cancellation_price.required' => 'El campo Precio por cancelacion es obligatorio',
          'cancellation_price.numeric' => 'El campo Precio por cancelacion solo debe de contener numeros',
          
           'min_price_per_distance.required' => 'El campo Precio minimo por distancia es obligatorio',
          'min_price_per_distance.numeric' => 'El campo Precio minimo por distancia solo debe de contener numeros',
           'max_price_per_distance.required' => 'El campo Precio maximo por distancia es obligatorio',
          'max_price_per_distance.numeric' => 'El campo Precio maximo por distancia solo debe de contener numeros',
                'base_fare_per_minute.required' => 'El campo Tarifa base por minuto es obligatorio',
          'base_fare_per_minute.numeric' => 'El campo Tarifa base por minuto solo debe de contener numeros',
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $file = Request::file('img_src');
         if($file){
          $usuario = Vehicle_Type::create($inputs);
          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $usuario->vehicle_type_id;
          $archive = "img/vehicle_type/vehicle_type".$id.".".$extension;

          if($file->move("img/vehicle_type", $archive)){
            $usuario->img_src = $archive;
            $usuario->save();
            session()->flash('success','Tipo de Vehiculo creado!');
            return Redirect::to('plataforma/Vehicle_Type');
          }else{
            session()->flash('notice','Ocurrió un error al subir la imagen!');
            return Redirect::to('plataforma/Vehicle_Type');
          }


        }else{
          session()->flash('notice','No se encontró la imagen!');
          return Redirect::to('plataforma/Vehicle_Type');
        }

      }
  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Vehicle_Type::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Vehicle_Type::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'state' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
       $city=City::all();
      return view('plataforma.vehicle_Type.save',compact('city'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $inputs = Request::all();
      $rules = [
           'name' => 'required|alpha|min:4|max:100',
             'max_persons' => 'required|numeric',
             'base_fare' => 'required|numeric',
             'min_fare' => 'required|numeric',
             'price_per_min' => 'required|numeric',
             'cancellation_price' => 'required|numeric',
            
            'min_price_per_distance' => 'required|numeric',
            'max_price_per_distance' => 'required|numeric',
            'base_fare_per_minute' => 'required|numeric',
        ];
     $messages = [
          'name.required' => 'El campo nombre es obligatorio',
          'name.min' => 'El nombre debe tener al menos 4 caracteres',
          'name.alpha' => 'El nombre solo debe de contener letras',
          'name.max' => 'El nombre debe maximo 100 caracteres',
          'max_persons.required' => 'El campo numero maximo de personas es obligatorio',
          'max_persons.numeric' => 'El campo numero maximo de personas solo debe de contener numeros',
          'base_fare.required' => 'El campo Tarifa base es obligatorio',
          'base_fare.numeric' => 'El campo Tarifa base solo debe de contener numeros',
          'min_fare.required' => 'El  campo Tarifa minima base_fare es obligatorio',
          'min_fare.numeric' => 'El campo Tarifa minima base_fare solo debe de contener numeros',
          'price_per_min.required' => 'El campo Precio por minuto es obligatorio',
          'price_per_min.numeric' => 'El campo Precio por minuto solo debe de contener numeros',
          'cancellation_price.required' => 'El campo Precio por cancelacion es obligatorio',
          'cancellation_price.numeric' => 'El campo Precio por cancelacion solo debe de contener numeros',
          
           'min_price_per_distance.required' => 'El campo Precio minimo por distancia es obligatorio',
          'min_price_per_distance.numeric' => 'El campo Precio minimo por distancia solo debe de contener numeros',
           'max_price_per_distance.required' => 'El campo Precio maximo por distancia es obligatorio',
          'max_price_per_distance.numeric' => 'El campo Precio maximo por distancia solo debe de contener numeros',
                'base_fare_per_minute.required' => 'El campo Tarifa base por minuto es obligatorio',
          'base_fare_per_minute.numeric' => 'El campo Tarifa base por minuto solo debe de contener numeros',
      ];
     $validar = Validator::make($inputs, $rules, $messages);
      Request::flash();
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $file = Request::file('img_src');
        $banner = Vehicle_Type::findOrFail($id);
        if($file){

          $archive = "";
          $extension = $file->getClientOriginalExtension();
          $id = $banner->vehicle_type_id;
          $archive = "img/Vehicle_Type/Vehicle_Type".$id.".".$extension;

          if($file->move("img/Vehicle_Type", $archive)){

        $banner->fill($inputs)->save();

            session()->flash('success','Vehicle_Type actualizado!');
            return Redirect::to('plataforma/Vehicle_Type');

          }else{

             $banner->fill($inputs)->save();
            session()->flash('notice','Ocurrio un problema al subir la imagen!');
            return Redirect::to('plataforma/Vehicle_Type');
          }

        }else{
            $banner->fill($inputs)->save();
          session()->flash('success','Vehicle_Type actualizado!');
          return Redirect::to('plataforma/Vehicle_Type');

        }
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Vehicle_Type::destroy($id);
    return redirect()->to('plataforma/Vehicle_Type');
    }
}
