<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Agreement;
use fraktalwebproject\Organization;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;
use Carbon\Carbon;

class AgreementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Agreement::all();
         $nState = Organization::all();
      return view('plataforma.agreement.index',compact('us','nState'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Agreement();
      $pos = [];
      for ($i=1; $i <= count(Agreement::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'agree' => $datos,
        'posiciones' => $pos
      ];
        $organizacion=Organization::all();  
      // return $data;
      return view('plataforma.agreement.save',compact('organizacion'))->with($data);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
$date = str_replace("-", "", $inputs['agreement_termination_date']);
$inputs['agreement_termination_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
            'agreement_termination_date' => 'required|date_format:Y-m-d',
            'vehicles_per_week' => 'required|numeric',
            'discount_per_travel' => 'required|numeric',
          
        ];
     $messages = [
          
          
          'agreement_termination_date.required' => 'El campo Fecha de vencimiento del convenio es obligatorio',
          'agreement_termination_date.date_format' => 'En el campo Fecha de vencimiento del convenio debe de terner el formato yyyy-mm-dd',
          'vehicles_per_week.required' => 'El campo Vehiculos por año año es obligatorio',
          'vehicles_per_week.numeric' => 'El campo Vehiculos por año solo debe de contener numeros',
           'discount_per_travel.required' => 'El campo Vehiculos por año año es obligatorio',
          'discount_per_travel.numeric' => 'El  campo Vehiculos por año solo debe de contener numeros',
      ];
     $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $cities = Agreement::create($inputs);
        if($cities){
          session()->flash('success','¡Convenio Creada!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear un Convenio, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Agreement');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Agreement::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Agreement::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'agree' => $datos,
        'posiciones' => $pos
      ];
      $organizacion=Organization::all();  
      // return $data;
      return view('plataforma.agreement.save',compact('organizacion'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $inputs = Request::all();
          $date = str_replace("-", "", $inputs['agreement_termination_date']);
$inputs['agreement_termination_date'] = Carbon::parse($date)->format('Y-m-d');

      $rules = [
               
            'agreement_termination_date' => 'required|date_format:Y-m-d',
            'vehicles_per_week' => 'required|numeric',
            'discount_per_travel' => 'required|numeric',
          
        ];
     $messages = [
          'agreement_termination_date.required' => 'El campo Fecha de vencimiento del convenio es obligatorio',
          'agreement_termination_date.date_format' => 'En el campo Fecha de vencimiento del convenio debe de terner el formato yyyy-mm-dd',
          'vehicles_per_week.required' => 'El campo Vehiculos por año año es obligatorio',
          'vehicles_per_week.numeric' => 'El campo Vehiculos por año solo debe de contener numeros',
           'discount_per_travel.required' => 'El campo Vehiculos por año año es obligatorio',
          'discount_per_travel.numeric' => 'El  campo Vehiculos por año solo debe de contener numeros',
      ];
        $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $ciudades = Agreement::findOrFail($id);
        $ciudades->fill($inputs)->save();
         return redirect()->to('plataforma/Agreement');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Agreement::destroy($id);
    return redirect()->to('plataforma/Agreement');
    }
}
