<?php

namespace fraktalwebproject\Http\Controllers;
use fraktalwebproject\Passenger;
use fraktalwebproject\Payment_Type;
use fraktalwebproject\User;
use Illuminate\Http\Request as Req;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Request;
use Hash;
use File;
use Auth;
use DB;

class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$request->user()->authorizeRoles('admin');
         $us=Passenger::all();
         $pt = Payment_Type::all();
         $fku = User::all();
      return view('plataforma.passenger.index',compact('us','pt','fku'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {  
      $datos = new Passenger();
      $pos = [];
      for ($i=1; $i <= count(Passenger::all()) + 1 ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
        $state=Payment_Type::all();
        $fku = User::all();
      // return $data;
      return view('plataforma.passenger.save',compact('state','fku'))->with($data);
      
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = Request::all();
      $rules = [
            
            'lat' => 'required|min:4|max:30',
            'lng' => 'required|min:4|max:30',
         
     
          
        ];
     $messages = [
          
          'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'lng.required' => 'El campo Longitud es obligatorio',
          'lng.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'lng.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $cities = Passenger::create($inputs);
        if($cities){
          session()->flash('success','¡Ciudad Creada!');
          
        }else{
          session()->flash('notice','¡Ocurrio un error al crear city, intentalo de nuevo!');
        }
            return redirect()->to('plataforma/Passenger');
          }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $datos = Passenger::findOrFail($id);
      $pos = [];
      for ($i=1; $i <= count(Passenger::all()) ; $i++) {
        $pos[$i] = $i;
      }
      $data = [
        'city' => $datos,
        'posiciones' => $pos
      ];
      // return $data;
      
        $state=Payment_Type::all();
        $fku = User::all();
      return view('plataforma.passenger.save',compact('state','fku'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

          $inputs = Request::all();
      $rules = [
            
            'lat' => 'required|min:4|max:30',
            'lng' => 'required|min:4|max:30',
           
           
          
        ];
     $messages = [
      
          'lat.required' => 'El campo Latitud es obligatorio',
          'lat.min' => 'El campo Latitud debe tener al menos 4 caracteres',
          'lat.max' => 'El campo Latitud debe tener maximo 30 caracteres',
          'lng.required' => 'El campo Longitud es obligatorio',
          'lng.min' => 'El campo Longitud debe tener al menos 4 caracteres',
          'lng.max' => 'El campo Longitud debe tener maximo 30 caracteres',
          
      ];
      $validar = Validator::make($inputs, $rules, $messages);
      if($validar->fails()){
        return Redirect::back()->withInput(Request::all())->withErrors($validar);
      }else{
        $ciudades = Passenger::findOrFail($id);
        $ciudades->fill($inputs)->save();
         return redirect()->to('plataforma/Passenger');
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    Passenger::destroy($id);
    return redirect()->to('plataforma/Passenger');
    }
}
