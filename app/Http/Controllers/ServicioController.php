<?php

namespace fraktalwebproject\Http\Controllers;
use Illuminate\Auth\Middleware;
use Illuminate\Http\Request;
use fraktalwebproject\Servicio;
use fraktalwebproject\Http\Requests\StoreTrainerRequest;


class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $Servicios=Servicio::all();
      return view('servicios.index',compact('Servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if ($request->hasFile('avatar')) { $file = $request->file('avatar'); 
        $name = time().$file->getClientOriginalName(); $file->move(public_path().'/images/', $name); }
          $Servicios=new Servicio();
        //estancia de Triner
       $Servicios->nombre=$request->input('name');
        $Servicios->posicion=$request->input('Posicion');
        $Servicios->img_url =$name;
        //metodo save para que se almacene un nuevo recurso en la bd
        $Servicios->save();
        //return 'saved';
        return 'guardado';   

         }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
         $servicios=Servicio::where('nombre','=',$name)->firstOrFail();
      return view('Servicios.edit',compact('servicios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
       $servicio=$request->except(['_token','_method']);
   Servicio::where('nombre',$name)->update($servicio);
       
       return 'update';     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id_servicio)
    {
      Servicio::destroy($id_servicio);
    }
}
