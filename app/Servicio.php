<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Servicio extends Model
{
	protected $primaryKey='id_servicio';
     protected $fillable=['nombre','posicion',
  'img_url'];

  public function getRouteKeyName(){
  	return 'id_servicio';
  }
}
