<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Integrante extends Model
{
	protected $primaryKey='id_integrante';
     protected $fillable=['nombre',
  'email','img_url'];
  
  protected $guarded=[];
  protected $hidden=[];

  public function getRouteKeyName(){
  	return 'id_integrante';
  }
}
