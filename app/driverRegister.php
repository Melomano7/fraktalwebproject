<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class driverRegister extends Model
{
  protected $table = 'drivers';
    protected $primaryKey = 'driver_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'lng','lat','vehicle_id','driver_licence','license_expiry_date','banck_account',
      'about','home_address','driver_type','name','email','city_id','password','phone'
    ];

     public function Vehicle(){
     	return $this->hasMany('fraktalwebproject\Vehicle', 'vehicle_id', 'vehicle_id');
    }
}
