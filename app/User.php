<?php

namespace fraktalwebproject;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable{
    use Notifiable;

    protected $table = 'users';
    protected $primaryKey = 'user_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'rol_id', 'img_src','city_id','phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function rol(){
      return $this->hasOne('fraktalwebproject\Roles','id_rol','id_rol');
    }

    public function driver(){
      return $this->belongsTo('fraktalwebproject\Driver','user_id','user_id');
    }

}
