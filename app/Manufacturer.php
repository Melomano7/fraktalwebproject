<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{
   protected $table = 'manufacturers';
    protected $primaryKey = 'manufacturer_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
}
