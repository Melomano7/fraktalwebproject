<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Vehicle_Model extends Model
{
     protected $table = 'vehicle__models';
    protected $primaryKey = 'vehicle_model_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','manufacturer_id'
    ];
}
