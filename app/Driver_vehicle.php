<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Driver_vehicle extends Model
{
  protected $table = 'driver_vehicles';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'vehicle_id','driver_id'
    ];

     public function Vehicle(){
     	return $this->hasMany('fraktalwebproject\Vehicle', 'vehicle_id', 'vehicle_id');
    }
}
