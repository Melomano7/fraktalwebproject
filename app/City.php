<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
   protected $table = 'cities';
   protected $primaryKey = 'city_id';
   protected $fillable = [
        'name', 'lat', 'ing', 'km_range', 'state_id','status'
    ];

    public function state(){
        return $this->belongsTo('fraktalwebproject\State','state_id','state_id');
    }
}
