<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
 protected $table = 'organizations';
   protected $primaryKey = 'organizations_id';
   protected $fillable = [
        'name', 'rfc', 'address', 'city_id', 'cp','company_representative','position_company_representative',
    ];
}
