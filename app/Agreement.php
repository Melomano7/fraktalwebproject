<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
  protected $table = 'agreements';
    protected $primaryKey = 'agreement_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'organizations_id','agreement_termination_date','vehicles_per_week','discount_per_travel'
    ];
}
