<?php

namespace fraktalwebproject;

use Illuminate\Database\Eloquent\Model;

class Passenger extends Model
{
   protected $table = 'passengers';
    protected $primaryKey = 'passenger_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lat','lng','payment_type_id','user_id','status'
    ];
}
