<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJourneysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journeys', function (Blueprint $table) {
            $table->increments('journey_id');
             $table->integer('passenger_id');
              $table->integer('driver_id');
            $table->string('cancel_motive',100);
             $table->date('cancel_date');
            $table->integer('duration');
             $table->float('distance_in_mts');
              $table->integer('city_id');
            $table->integer('pick_waypoint');
            $table->integer('drop_waypoint');
            $table->string('remarks',200);
            $table->integer('booling_id');
            $table->integer('vehicle_type');
            $table->integer('vehicle_id');
            $table->date('complete_date');
            $table->string('cancel_person',30);
            $table->date('arrive_date');
            $table->date('starter_date');
            $table->float('price_per_distance');
            $table->float('price_per_minute');
            $table->float('total_cancelation');
            $table->float('total');
            $table->integer('payment_id');
            $table->float('max_price_per_distance');
            $table->float('min_price_per_distance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journeys');
    }
}
