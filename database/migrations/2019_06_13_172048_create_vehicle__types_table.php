<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle__types', function (Blueprint $table) {
            $table->increments('vehicle_type_id');
            $table->string('name');
            $table->integer('max_persons');
            $table->float('base_fare');
            $table->float('min_fare');
            $table->float('price_per_min');
            $table->float('cancellation_price');
            $table->string('img_src',200)->nullable($value = true);
            $table->float('min_price_per_distance');
            $table->float('max_price_per_distance');
            $table->float('base_fare_per_minute');
            $table->integer('city_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle__types');
    }
}
