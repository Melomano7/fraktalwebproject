<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('name',100)->nullable($value = false);
            $table->string('email',100)->unique()->nullable($value = false);
            $table->string('password',100);
            $table->string('img_src',100)->nullable($value = true);
            $table->string('login_type',100)->nullable($value = true);
            $table->string('facebook_id',400)->nullable($value = true);
            $table->string('fcm_token',400)->nullable($value = true);
            $table->integer('city_id')->nullable($value = true);
            $table->string('android_version',10)->nullable($value = true);
            $table->string('ios_version',10)->nullable($value = true);
            $table->integer('rol_id')->nullable($value = true);
            $table->date('last_login')->nullable($value = true);
            $table->string('phone',13)->nullable($value = true);
            
            $table->string('app_android_version',10)->nullable($value = true);
            $table->string('ios_android_version',10)->nullable($value = true);
            $table->char('baja')->default($value = 0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
