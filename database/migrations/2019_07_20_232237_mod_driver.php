<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::table('drivers',function (Blueprint $table) {
    
    $table->string('lat',30)->default($value = '')->change();
    $table->string('lng',30)->default($value = '')->change();
    $table->integer('vehicle_id')->default($value = 0)->change();
    $table->string('driver_licence_img_src',200)->nullable($value = true)->change();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
