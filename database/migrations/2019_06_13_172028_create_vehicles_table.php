<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('vehicle_id');
            $table->string('title',30);
            $table->string('status',30);
            $table->string('vehicle_model_id',30);
            $table->integer('manufacturer_id');
             $table->string('color',30);
            $table->string('insurance_no',30);
            $table->date('insurance_expiry_date');
            $table->integer('city_id')->nullable($value = true);
            $table->float('road_taxt');
            $table->string('plate',30);
            $table->integer('vehicle_type_id');
            $table->integer('year');
            $table->string('img_src',200)->nullable($value = true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
