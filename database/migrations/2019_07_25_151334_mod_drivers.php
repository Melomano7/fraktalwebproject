<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::table('drivers',function (Blueprint $table) {
    $table->string('lat',30)->nullable($value = true)->change();
    $table->string('lng',30)->nullable($value = true)->change();
    $table->integer('grades')->nullable($value = true)->change();
    $table->integer('speed')->nullable($value = true)->change();
    $table->string('password',30)->nullable($value = true)->change();
   $table->string('operating_area',30)->nullable($value = true)->change();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
