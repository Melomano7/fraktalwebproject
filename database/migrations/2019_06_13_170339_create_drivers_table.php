<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('driver_id');
            $table->string('status',30);
            $table->string('lat',30);
            $table->string('lng',30);
            $table->integer('vehicle_id');
            $table->string('driver_licence',30);
            $table->string('driver_licence_img_src',200)->nullable($value = false);
            $table->date('license_expiry_date');
            $table->string('banck_account',20);
            $table->string('about',700);
        
            $table->string('operating_area',30);
            $table->string('home_address',200);
            $table->integer('grades');
            $table->integer('speed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
