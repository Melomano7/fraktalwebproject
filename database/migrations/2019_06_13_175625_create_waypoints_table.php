<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWaypointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waypoints', function (Blueprint $table) {
            $table->increments('waypoint_id');
            $table->string('lat',30);
            $table->string('ing',30);
            $table->string('address',30);
            $table->string('google_place_id',30);
            $table->integer('journey_id');
            $table->integer('position');
            $table->integer('number');
            $table->string('street',100);
            $table->string('col',100);
            $table->integer('cp');
            $table->integer('city_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waypoints');
    }
}
