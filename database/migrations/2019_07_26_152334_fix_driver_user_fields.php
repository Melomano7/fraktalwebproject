<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixDriverUserFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('drivers', function (Blueprint $table) {
            // We already have these fields in the user table, it's just to relate the tables
            $table->dropColumn('name');
            $table->dropColumn('email');
            $table->dropColumn('city_id');
            $table->dropColumn('password');
            $table->dropColumn('phone');
            // Esta es la relacion
            $table->integer('user_id')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
