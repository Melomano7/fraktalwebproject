<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificarDriver extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::table('drivers',function (Blueprint $table) {
    $table->string('email',30);
    $table->integer('city_id');
    $table->string('password',30);
    $table->string('phone',15);
    $table->string('status',30)->default($value = 'Por verificar')->change();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     
    }
}
