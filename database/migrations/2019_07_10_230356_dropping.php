<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Dropping extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
    Schema::table('organizations',function (Blueprint $table) {
    $table->string('company_representative',30);
    $table->string('position_company_representative',30);
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
   Schema::table('agreements', function (Blueprint $table) {
    $table->dropColumn(['company_representative', 'position_company_representative']);
});
    }
}
