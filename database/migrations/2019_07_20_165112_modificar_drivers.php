<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModificarDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      
    Schema::table('vehicles',function (Blueprint $table) {
    $table->integer('driver_id');  
});
    Schema::table('drivers',function (Blueprint $table) {
    $table->string('driver_type',30);
    
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
   Schema::table('drivers', function (Blueprint $table) {
    $table->dropColumn(['vehicle_type_id']);
});
    }
}
