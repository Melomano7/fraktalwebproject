<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('organizations_id');
            $table->string('name',100)->nullable($value = false);
            $table->string('rfc',30);
             $table->string('address',30);
             $table->integer('city_id');
             $table->integer('cp');
            //  $table->string('company_representative',30);
            // $table->string('position_company_representative',30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
